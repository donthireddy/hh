# -*- coding: utf-8 -*-
"""Utilities to Find optimal Hackers League lineup

"""
import json
from dotmap import DotMap
from functools import lru_cache
from combinatorics import *

FORMAT_PREF_SCORE = 100
IDEAL_PARTNER_SCORE = 100
NTRP_SCALING = 100
REPEATED_MATCH_PENALTY = 400
SINGLES = 'S'
DOUBLES = 'D'


# Utility function
# returns a generator that can be used to iterate over
# all possible pairs of given items
def enumerate_pairs(items, group_size=2):
    if not items:
        return []
    num_groups = int(len(items)/group_size)
    odd_players = len(items) % group_size
    group_sizes = [group_size] * num_groups
    if odd_players:
        group_sizes += [odd_players]
    #print("DBG items=", items, " groupsizes = ", group_sizes)
    return m_way_unordered_combinations(items, group_sizes)


# Given a list of players, returns a generator
# of all possible doubles match lineups among these players
def enumerate_doubles_lineups(players):
    #print("DBG doubles_lineups", players)
    if not players:
        return []
    # first make all possible doubles teams
    for teams in enumerate_pairs(players):
        # then make all possible matchups among each set of teams
        for lineup in enumerate_pairs(teams):
            yield lineup


#################################
# Compute the desireability score of a singles lineup
def score_singles(D, lineup, verbose=False):
    value = 0
    if verbose: print("<< Evaluating Singles Lineup", lineup)
    #
    # 1. Score based on how many of the players play their preferred format
    fscore = 0
    for match in lineup:
        if len(match) > 1:
            for player in match:
                fscore += format_score(D, player, SINGLES)
    if verbose: print("Lineup format pref score", fscore)
    value += fscore

    # 2. Match parity score (in other words, how balanced are the two sides?)
    pscore = 0
    for m in lineup:
        if len(m) > 1:
            pscore += singles_parity_score(D, m)
    value += pscore
    if verbose: print("Lineup parity score", pscore)

    # 3. Penalize if they have already played before in the league
    rscore = 0
    for match in lineup:
        if len(match) > 1:
            rscore += singles_repetition_score(D, match)
    if verbose: print("Lineup repeated match score", rscore)
    value += rscore

    if verbose: print("Lineup Total score", value)
    return value


##############################
# Compute the desireability score of a doubles lineup
def score_doubles(D, lineup, verbose=False):
    value = 0
    lineup = list(lineup)
    if verbose: print("<< Evaluating doubles lineup", lineup)
    #
    # 1. Score based on how many of the players play their preferred format
    fscore = 0
    for m in lineup:
      if len(m) > 1:
        for side in m:
         if len(side) > 1:
          for player in side:
            fscore += format_score(D, player, DOUBLES)
    if verbose: print("  Lineup Format score", fscore)
    value += fscore

    # 2. Partner preference. Lineups where people play with their
    #    preferred partners are better than those where they don't
    pref_score = 0
    for m in lineup:
     if len(m) > 1:
      for side in m:
         if len(side) > 1:
           pref_score += + partner_score(D, side[0], side[1]) + partner_score(D, side[1], side[0])

    if verbose: print("  Lineup Partner pref score", pref_score)
    value += pref_score

    # 3. Match parity score (in other words, how balanced are the two sides)
    pscore = 0
    for m in lineup:
        if len(m) > 1 and len(m[0])> 1 and len(m[1]) > 1:
          pscore += doubles_parity_score(D, m)
    value += pscore
    if verbose: print("  Lineup parity score", pscore)

    # 4. Penalize if they have already played before in the league
    rscore = 0
    for m in lineup:
        if len(m) > 1 and len(m[0])> 1 and len(m[1]) > 1:
            rscore += doubles_repetition_score(D, m)
    value += rscore
    if verbose: print("  Lineup repeated match score", rscore)

    return value


##############################
# 1. Does player get singles/doubles preference?
def format_score(D, player, format, verbose=False):
     if D.format_prefs[player] == format:
            ret = FORMAT_PREF_SCORE
     else:
            ret = -FORMAT_PREF_SCORE
     if verbose: print("    fmt pref score", player, format, ret)
     return ret

##############################
# 2. How much does player x like y
def partner_score(D, x, y, verbose=False):
    prefs = D.partner_prefs.get(x, [None, None, None])
    if y == prefs[0]:
        ret = IDEAL_PARTNER_SCORE
    elif y == prefs[1]:
        ret = IDEAL_PARTNER_SCORE * .75
    elif y == prefs[2]:
        ret = IDEAL_PARTNER_SCORE * .25
    else:
        ret = 0
    if verbose: print("    partner pref score ", x, y, ret)
    return ret

##############################
# 3. How balanced is the match in terms of the NTRP
#    ratings of the two sides?
def doubles_parity_score(D, match,verbose=False):
    @lru_cache()
    def get_rating(p):
        if D.player_ratings.has_key(p):
            return D.player_ratings[p]
        else:
            return 3.5
    hr1, hr2 = get_rating(match[0][0]), get_rating(match[0][1])
    ar1, ar2 = get_rating(match[1][0]), get_rating(match[1][1])

    # Eg: if a 3.35 and a 3.10 play a 3.30 and a 3.11,
    #  diff = (3.35 + 3.10  -  ( 3.30 + 3.11)) = 0.04
    # parity score for the match = 100 - 100*.04 = 96

    diff = hr1 + hr2 - ar1 - ar2
    ret = NTRP_SCALING*2 -  NTRP_SCALING * abs(diff)
    if verbose: print("    parity score", match, "%.2f + %.2f vs %.2f + %.2f ->" % (hr1, hr2, ar1, ar2), ret)
    return ret


def singles_parity_score(D, match, verbose=False):
    @lru_cache()
    def get_rating(p):
        if D.player_ratings.has_key(p):
            return D.player_ratings[p]
        else:
            return 3.5

    r1, r2 = get_rating(match[0]), get_rating(match[1])
    # Eg: if a 3.35 and a 3.10 play a 3.30 and a 3.11,
    #  diff = (3.35 + 3.10  -  ( 3.30 + 3.11)) = 0.04
    # parity score for the match = 100 - 100*.04 = 96
    ret = NTRP_SCALING - NTRP_SCALING * abs(r1 - r2)
    if verbose: print("    parity score", match, "%.2f vs %.2f ->" % (r1, r2), ret)
    return ret

##############################
# 4. Negative score if this match has been scheduled
#    previously, to encourage our algorithm to schedule
#    a variety of opponents for each player over the season.
def doubles_repetition_score(D, match, verbose=False):
    h, a = match
    h1, h2 = h
    a1, a2 = a
    N = 0
    for h1_, h2_, a1_, a2_ in D.prev_doubles_matches:
        if (({h1_,h2_} == {h1,h2} and {a1_, a2_} == {a1, a2}) or
            ({h1_,h2_} == {a1,a2} and {a1_, a2_} == {h1, h2})):
            N += 1
    ret = -REPEATED_MATCH_PENALTY * N
    if verbose: print("    rematch penalty for", match, ret)
    return ret

def singles_repetition_score(D, match, verbose=False):
    h, a = match
    N = 0
    for h1_, h2_, a1_, a2_ in D.prev_singles_matches :
        if ((h1_ == h and a1_ == a) or
            (h1_ == a and a1_ == h)):
            N += 1
    ret = -REPEATED_MATCH_PENALTY * N
    if verbose: print("    rematch penalty for", match, ret)
    return ret

# Find optimal singles lineup
def schedule_singles(D, verbose=False):
    best_score = -9999
    best_lineup = []
    num_lineups = 0

    #print ("DBG schedule_singles", D.singles_signups)
    for lineup in enumerate_pairs(D.singles_signups):
        num_lineups += 1
        this_score = score_singles(D, lineup, verbose=False)
        if verbose: print(">> Lineup score =", this_score,"\n")
        if this_score > best_score:
            best_score = this_score
            best_lineup = lineup
    if verbose: print("### Best score out of %d lineups = %.2f\n" % (num_lineups, best_score))
    return best_lineup


# Find optimial doubles lineup
def schedule_doubles(D, verbose=False):
    best_score = -9999
    best_lineup = []
    num_lineups = 0

    for l in enumerate_doubles_lineups(D.doubles_signups):
        lineup = list(l)
        num_lineups += 1
        this_score = score_doubles(D, lineup, verbose=False)
        if verbose: print(">> Lineup score =", this_score, "\n")
        if this_score > best_score:
            best_score = this_score
            best_lineup = lineup
    if verbose: print("### Best score out of %d lineups = %.2f\n" % (num_lineups, best_score))
    return list(best_lineup)

def rm_redundancy(D):
    """
    Remove data that will never be used
    This should make the optimization faster
    """
    all_signups = D.singles_signups + D.doubles_signups
    D.partner_prefs = {
        (k,v) for (k,v) in D.partner_prefs.items()
        if k in all_signups
    }
    D.player_ratings = {
        (k,v) for (k,v) in D.player_ratings.items()
        if k in all_signups
    }

    return D

def do_it(D, verbose=False):
    # If we have an odd numnber of singles signups *and*
    # doubles signups are not a multiple of 4,
    # we may Try to move people around
    # If the total number of odd players is 4,
    # we can have an extra doubles match and keep everyone playing
    # If the total numer of odd players is 2 or 3,
    # we can get an extra singles match.
    #D = rm_redundancy(D)
    singles_odd_num = len(D.singles_signups) % 2
    doubles_odd_num = len(D.doubles_signups) % 4
    odd_num = singles_odd_num + doubles_odd_num
    do_singles_first = (odd_num == 4)

    final_singles_lineup = []
    final_doubles_lineup = []
    if do_singles_first:
        slineup = schedule_singles(D)
        for match in slineup:
            if len(match) == 1:
                if verbose: print("Sending odd singles player %s to doubles" % (match[0],))
                D.doubles_signups.append(match[0])
            else:
                final_singles_lineup.append(match)
        final_doubles_lineup = schedule_doubles(D)
    else:
        dlineup = schedule_doubles(D)
        for match in dlineup:
            if len(match) < 2 or len(match[0]) < 2 or len(match[1]) < 2:
                for team in match:
                    for player in team:
                        if verbose: print("Sending odd doubles player %s to doubles" % (player,))
                        D.singles_signups.append(player)
            else:
                final_doubles_lineup.append(match)
        final_singles_lineup = schedule_singles(D)

    return final_singles_lineup, final_doubles_lineup


########################################################################
# Main
########################################################################
if __name__ == "__main__":
    # Read in data output by get_scheduling_data.py
    # or hand edited
    D = DotMap(json.load(open("schedule.dat")))

    final_singles_lineup, final_doubles_lineup = do_it(D)

    print("Best singles lineup", final_singles_lineup)
    print("Best doubles lineup", final_doubles_lineup)
