from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.views.generic import TemplateView
#import allauth
from django.contrib import admin
from django.contrib.auth.models import User, Group
from tennis.models import Registration
from rest_framework import viewsets, routers
from memoize import memoize
from rest_framework_jwt.views import obtain_jwt_token

admin.autodiscover()

class SimpleStaticView(TemplateView):
    def get_template_names(self):
        return [self.kwargs.get('template_name') + ".html"]
    
    def get(self, request, *args, **kwargs):
        from django.contrib.auth import authenticate, login
        if request.user.is_anonymous():
            # Auto-login the User for Demonstration Purposes
            user = authenticate()
            login(request, user)
        return super(SimpleStaticView, self).get(request, *args, **kwargs)

HACKERS_GROUP_ID=1
LEAGUE_3518= 26
LEAGUE_3540=27
LEAGUE_4018=24
LEAGUE_4040=25

class HomeView(TemplateView):
    template_name = 'homepage.html'

    @memoize(timeout=10)
    def get_maillist(self, group_id=HACKERS_GROUP_ID):
        return ",".join([u.email for u in 
                         Group.objects.filter(id=group_id)[0].
                         user_set.filter(is_active=True)])

    @memoize(timeout=10)
    def get_usta_maillist(self, league_id):
        return ",".join([r.player.user.email for r in Registration.objects.filter(league__id=league_id)])


    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['member_emails'] = self.get_maillist()
        context['member_emails_3518'] = self.get_usta_maillist(LEAGUE_3518)
        context['member_emails_3540'] = self.get_usta_maillist(LEAGUE_3540)
        context['member_emails_4018'] = self.get_usta_maillist(LEAGUE_4018)
        context['member_emails_4040'] = self.get_usta_maillist(LEAGUE_4040)
        return context

# ViewSets define the view behavior.
#class UserViewSet(viewsets.ModelViewSet):
#    model = User

#class GroupViewSet(viewsets.ModelViewSet):
#    model = Group
# Routers provide an easy way of automatically determining the URL conf
#router = routers.DefaultRouter()
#router.register(r'users', UserViewSet)
#router.register(r'groups', GroupViewSet)


urlpatterns = [
    url(r"^$", HomeView.as_view(), name="home"),
    url(r"contact^$", HomeView.as_view(), name="contact"),
    #url(r"^react", HomeView.as_view(), name="react"),
    url(r"^react/", TemplateView.as_view(template_name="react.html"), name="react"),
    url(r"wht_2013", TemplateView.as_view(template_name="wht_2013.html"), name="wht_2013"),
    url(r"^grappelli/", include('grappelli.urls')),
    url(r"^admin/", include(admin.site.urls)),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r"^accounts?/", include("account.urls")),
    #All Auth URLS
    #url(r'^accounts/', include('allauth.urls')),
    url(r'^chat/', include('chat.urls')), 
    url(r'^tennis/', include('tennis.urls')), 
    url(r'^api/', include('tennis_api.urls')), 

    #url(r'^rtr/', include(router.urls)),
    # To get token from django-rest-framework (no -jwt)
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # JWT
    # ===
    # To get JWT token from djang-rest-framework-jwt 
    # eg:
    #    curl -X POST -d "username=murali&password=MySecret" https://hh.donthireddy.us/api-auth/login/
    # You should get back a JWT like this: {"token":"<header>.<payload>.<signature>"}
    # Then use the token to make queries
    #    curl -H "Authorization: JWT <header>.<payload>.<signature>" GET https://hh.donthireddy.us/api/facilities/
    #
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^(?P<template_name>\w+)$', SimpleStaticView.as_view(), name='mcp'),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
