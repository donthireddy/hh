import os,re,platform
import django
from django.utils.module_loading import import_string
import datetime

# Make this unique, and don't share it with anybody.
SECRET_KEY = "k7gg(wdx5#k@44y)r&amp;%d7l4-(rm2_28=d0u$y^w4gm#xrh*ym8"
ACCOUNT_TIMEZONES = [('America/New_York', 'America/New_York')]
ACCOUNT_LANGUAGES = [("en", "English")]
ACCOUNT_OPEN_SIGNUP = True
ACCOUNT_USE_OPENID = False
ACCOUNT_REQUIRED_EMAIL = True
ACCOUNT_EMAIL_UNIQUE = True
ACCOUNT_EMAIL_VERIFICATION = True
ACCOUNT_EMAIL_AUTHENTICATION = False
LOGIN_REDIRECT_URL = "home"
ACCOUNT_LOGIN_REDIRECT_URL = "home"
ACCOUNT_LOGOUT_REDIRECT_URL = "home"
ACCOUNT_SIGNUP_REDIRECT_URL = "home"
ACCOUNT_PASSWORD_CHANGE_REDIRECT_URL="home"
ACCOUNT_PASSWORD_RESET_REDIRECT_URL="home"
ACCOUNT_EMAIL_CONFIRMATION_ANONYMOUS_REDIRECT_URL='home'
ACCOUNT_EMAIL_CONFIRMATION_AUTHENTICATED_REDIRECT_URL='home'
ACCOUNT_EMAIL_CONFIRMATION_URL='home'
ACCOUNT_SETTINGS_REDIRECT_URL='home'
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 2
ACCOUNT_CREATE_ON_SAVE=True
ACCOUNT_PASSWORD_STRIP=True
ACCOUNT_USER_DISPLAY=lambda user: user.username
ACCOUNT_HOOKSET=import_string("account.hooks.AccountDefaultHookSet")()
ACCOUNT_PASSWORD_USE_HISTORY=False
ACCOUNT_NOTIFY_ON_PASSWORD_CHANGE=True
#EMAIL_VERIFICATION = True
#EMAIL_REQUIRED = True
ACCOUNT_REMEMBER_ME_EXPIRY=9999999

#def ACCOUNT_USER_DISPLAY(*x):
#   return 'fakeuser'
dbname='tennis'
dbhost='localhost'
IS_PRODUCTION = not not re.search('webfaction', platform.node())
if IS_PRODUCTION:
   dbname = 'tennis'
   dbhost = '' # localhost
   redis_port='20758'
else:
   redis_port='6379'   

PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
PACKAGE_ROOT = os.path.abspath(os.path.dirname(__file__))

DATABASES = {
#   'mysql': {
#      'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
#      'NAME': dbname,
#      'USER': 'donthireddy',                      # Not used with sqlite3.
#      'PASSWORD': 'donthireddy',                  # Not used with sqlite3.
#      'HOST': dbhost,                      # Set to empty string for localhost. Not used with sqlite3.
#      'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
#      'OPTIONS': {
#            'sql_mode': 'traditional',
#        }
#   },
   'default': {
      'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
      'NAME': dbname,
      'USER': 'donthireddy',                      # Not used with sqlite3.
      'PASSWORD': 'donthireddy',                  # Not used with sqlite3.
      'HOST': dbhost,                      # Set to empty string for localhost. Not used with sqlite3.
      'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
   },
#   'tennis2': {
#      'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
#      'NAME': 'tennis2',
#      'USER': 'donthireddy',                      # Not used with sqlite3.
#      'PASSWORD': 'donthireddy',                  # Not used with sqlite3.
#      'HOST': dbhost,                      # Set to empty string for localhost. Not used with sqlite3.
#      'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
#      'OPTIONS': {
#            'sql_mode': 'traditional',
#        }
#   }
}
if IS_PRODUCTION:
    #DEBUG = False
    DEBUG = True
    ADMINS = [
        ("Murali Donthireddy", "donthireddy@mariandrive.com"),
    ]
else:
    DEBUG = True
    ADMINS = [
    ]

CON_MAX_AGE = 120

MANAGERS = ADMINS

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = "America/New_York"

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = "en-us"

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

ALLOWED_HOSTS=['localhost:8000', '*']

if IS_PRODUCTION:
   STATIC_ROOT = "/home/donthireddy/static"
   MEDIA_URL = "https://static.mariandrive.com/media/"
   STATIC_URL = "https://static.mariandrive.com/"
else:
   STATIC_URL = "/static/"
   MEDIA_URL = "https://static.mariandrive.com/media/"
   STATIC_ROOT = "/opt/webserver"

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = [
   "django.contrib.staticfiles.finders.FileSystemFinder",
   "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]

STATICFILES_DIRS = (
    #This lets Django's collectstatic store our bundles
    os.path.join(PROJECT_ROOT, 'assets'), 
)

THEME_ACCOUNT_CONTACT_EMAIL='donthireddy@mariandrive.com'

MIDDLEWARE_CLASSES = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    "django.contrib.messages.middleware.MessageMiddleware",
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    "account.middleware.LocaleMiddleware",
    "account.middleware.TimezoneMiddleware",

    #"debug_toolbar.middleware.DebugToolbarMiddleware",
    #"tennis.disable.DisableCSRF",
]
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True
CSRF_TRUSTED_ORIGINS = [ 'localhost', '.donthireddy.us', '.mariandrive.com' ]
ROOT_URLCONF = "mcp.urls"

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = "mcp.wsgi.application"

TEMPLATES = [
{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'DIRS': [os.path.join(PACKAGE_ROOT, "templates")],
    'APP_DIRS': True,
    'OPTIONS': {
        'debug': DEBUG,
        'context_processors': [
          "django.contrib.auth.context_processors.auth",
          "django.template.context_processors.debug",
          "django.template.context_processors.i18n",
          "django.template.context_processors.media",
          "django.template.context_processors.static",
          "django.template.context_processors.tz",
          "django.template.context_processors.request",
          "django.contrib.messages.context_processors.messages",

          "account.context_processors.account",
          "django.template.context_processors.request",
          'hh_auth0.context_processors.auth0',


        ],
    },
},] 
INSTALLED_APPS = [
    "grappelli",
    "django_admin_bootstrapped",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.staticfiles",


    'django.contrib.admindocs',
    'django_extensions',

    # theme
    "pinax_theme_bootstrap",
    "bootstrapform",
    "pinax_theme_bootstrap_account",

    # external
    "account",
    "metron",
    "django_tables2",
    "crispy_forms",
    "betterforms",

    "memoize",
    "corsheaders",
    "rest_framework",
    "rest_framework.authtoken",
    "django_filters",
    "import_export",

    "dbbackup",
    "channels",
    "rest_framework_swagger",
    'hh_auth0',
    "rest_auth",
    #"allauth",
    #"allauth.account",
    "rest_auth.registration",
    #'allauth.socialaccount',
    #'allauth.socialaccount.providers.facebook',
    #'allauth.socialaccount.providers.twitter',

    "webpack_loader",

    "graphene_django",

    "chat", # Channels Example app

    "mcp",
    "tennis",
    "tennis_api",

]

GRAPHENE = {
  'SCHEMA': 'tennis.schema.schema'
}

if IS_PRODUCTION:
  DOMAIN='https://hh.mariandrive.com'
else:
  DOMAIN='https://hh.donthireddy.us'

AUTH0_DOMAIN = 'donthireddy.auth0.com'
AUTH0_CLIENT_ID = 'j6GJrB1AcObl9k5qts4aAK6JP7QDEYY5'
AUTH0_SECRET = 'O7QT5z6-H_0rKvd7E6s7bvNrHPAFDNTnQ_sETVAaAxwkzoF1ZPI5vIx-N4MVj8GE'
AUTH0_CALLBACK_URL = DOMAIN + '/tennis/auth/auth_callback'
AUTH0_SUCCESS_URL = DOMAIN + '/'

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "asgi_redis.RedisChannelLayer",
        "CONFIG": {
            "hosts": [os.environ.get('REDIS_URL', 'redis://localhost:' + redis_port )],
        },
        "ROUTING": "chat.routing.channel_routing",
    },
}

WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': 'bundles/',
        'STATS_FILE': os.path.join(PROJECT_ROOT, 'webpack-stats.json'),
    }
}
if IS_PRODUCTION:
#if True:
  WEBPACK_LOADER['DEFAULT'].update({
        'BUNDLE_DIR_NAME': 'dist/',
        'STATS_FILE': os.path.join(PROJECT_ROOT, 'webpack-stats-prod.json')
  })

DBBACKUP_STORAGE = 'django.core.files.storage.FileSystemStorage'
DBBACKUP_STORAGE_OPTIONS = {'location': os.path.abspath(os.path.join(PROJECT_ROOT,'backups'))}
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        #'rest_framework.authentication.BasicAuthentication',

        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    # Use hyperlinked styles by default.
    # Only used if the `serializer_class` attribute is not set on a view.
    'DEFAULT_MODEL_SERIALIZER_CLASS':
        'rest_framework.serializers.HyperlinkedModelSerializer',

    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}
from my_secrets import *
JWT_AUTH = {
    'JWT_ENCODE_HANDLER':
    'rest_framework_jwt.utils.jwt_encode_handler',

    'JWT_DECODE_HANDLER':
    'rest_framework_jwt.utils.jwt_decode_handler',

    'JWT_PAYLOAD_HANDLER':
    'rest_framework_jwt.utils.jwt_payload_handler',

    'JWT_PAYLOAD_GET_USER_ID_HANDLER':
    'rest_framework_jwt.utils.jwt_get_user_id_from_payload_handler',

    'JWT_RESPONSE_PAYLOAD_HANDLER':
    'rest_framework_jwt.utils.jwt_response_payload_handler',

    'JWT_SECRET_KEY': EMAIL_PASSWORD,
    'JWT_GET_USER_SECRET_KEY': None,
    'JWT_PUBLIC_KEY': None,
    'JWT_PRIVATE_KEY': None,
    'JWT_ALGORITHM': 'HS256',
    'JWT_VERIFY': True,
    'JWT_VERIFY_EXPIRATION': True,
    'JWT_LEEWAY': 0,
    'JWT_EXPIRATION_DELTA': datetime.timedelta(seconds=3600*24*365),
    'JWT_AUDIENCE': None,
    'JWT_ISSUER': None,

    'JWT_ALLOW_REFRESH': False,
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=7),

    'JWT_AUTH_HEADER_PREFIX': 'JWT',
    'JWT_AUTH_COOKIE': None,

}
AUTH_PROFILE_MODULE = 'tennis.Player'
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
#LOGGING = {
#    "version": 1,
#    "disable_existing_loggers": False,
#    "filters": {
#        "require_debug_false": {
#            "()": "django.utils.log.RequireDebugFalse"
#        }
#    },
#    "handlers": {
#        "mail_admins": {
#            "level": "ERROR",
#            "filters": ["require_debug_false"],
#            "class": "django.utils.log.AdminEmailHandler"
#        }
#    },
#    "loggers": {
#        "django.request": {
#            "handlers": ["mail_admins"],
#            "level": "ERROR",
#            "propagate": True,
#        },
#    }
#}

FIXTURE_DIRS = [
    os.path.join(PROJECT_ROOT, "fixtures"),
]

# This just writes emails to console
# EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"

DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.version.VersionDebugPanel',
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    'debug_toolbar.panels.headers.HeaderDebugPanel',
    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar.panels.sql.SQLDebugPanel',
    'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
)
INTERNAL_IPS = ('173.72.122.185', '127.0.0.1',)
#def custom_show_toolbar(request):
#    return True  # Always show toolbar, for example purposes only.

#THEME_ADMIN_URL='/admin'
#THEME_CONTACT_EMAIL='donthireddy@yahoo.com'
#ACCOUNT_HOOKSET = "account.hooks.AccountDefaultHookSet"

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
    #'SHOW_TOOLBAR_CALLBACK': custom_show_toolbar,
    #'EXTRA_SIGNALS': ['myproject.signals.MySignal'],
    'HIDE_DJANGO_SQL': False,
    'TAG': 'div',
    'ENABLE_STACKTRACES' : True,
}
AUTHENTICATION_BACKENDS = [
    "account.auth_backends.UsernameAuthenticationBackend",
    'hh_auth0.auth_backend.Auth0Backend',
]
DEFAULT_FROM_EMAIL='donthireddy@mariandrive.com'
EMAIL_SUBJECT_PREFIX = "[WHT] "
EMAIL_HOST = "smtp.webfaction.com" if IS_PRODUCTION else "smtp.gmail.com"
EMAIL_HOST_USER = "donthireddy"
EMAIL_PORT = 587
EMAIL_USE_TLS = True
# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

