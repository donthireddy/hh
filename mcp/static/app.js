angular.module('facility', ['restangular']).
  config(function($routeProvider, RestangularProvider) {
    $routeProvider.
      when('/', {
        controller:ListCtrl, 
        templateUrl:'list.html'
      }).
      when('/edit/:facilityId', {
        controller:EditCtrl, 
        templateUrl:'detail.html',
        resolve: {
          facility: function(Restangular, $route){
            return Restangular.one('facilities', $route.current.params.facilityId).get();
          }
        }
      }).
      when('/new', {controller:CreateCtrl, templateUrl:'detail.html'}).
      otherwise({redirectTo:'/'});
      
      var token = $.cookie('csrftoken');
      RestangularProvider.setDefaultHeaders({'X-CSRFToken': token});

      RestangularProvider.setBaseUrl('http://localhost:8000/tennis');
      
      RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
        
        if (operation === 'put') {
          elem._id = undefined;
          return elem;
        }
        return elem;
      })
  });


function ListCtrl($scope, Restangular) {
    Restangular.all("facilities/.json").getList().then(function (response) {
	$scope.facilities = response;     
    }, function (response) {
	console.log("Failed to find customers with status code", response.status);
    });
}


function CreateCtrl($scope, $location, Restangular) {
  $scope.save = function() {
    Restangular.all('facilities/').post($scope.facility).then(function(facility) {
      $location.path('/list');
    });
  }
}

function EditCtrl($scope, $location, Restangular, facility) {
  var original = facility;
  $scope.facility = Restangular.copy(original);
  

  $scope.isClean = function() {
    return angular.equals(original, $scope.facility);
  }

  $scope.destroy = function() {
    original.remove().then(function() {
      $location.path('/list');
    });
  };

  $scope.save = function() {
    $scope.facility.put().then(function() {
      $location.path('/');
    });
  };
}
