from . import models as M
import json
import math

# Player reperesenting a bye
bye = None # will be set later

class Match(object):
    def __init__(self, bracket, league_id, round_num):
        self.bracket = bracket
        self.db_match = None
        self.league_id = league_id
        self.round_num = round_num 
        self.home_team = None
        self.away_team = None
        self.upper_match = None
        self.lower_match = None
        self.winner = None
        self.home_won = None
        self.home_score = ""
        self.away_score = ""
    def __str__(self):
        return ("%d: L %d, R%d, %s vs. %s" % (
                self.id, self.league_id, self.round_num, 
                self.home_team, self.away_team))
    def db_lookup(self):
        global bye

        if self.upper_match and self.lower_match:
            self.home_team = self.upper_match.winner
            self.away_team = self.lower_match.winner

        if not self.home_team and not self.away_team:
            return
        # Deal with byes
        if bye == self.home_team:
            self.winner = self.away_team
            self.home_won = False
            #print "Bye for " + str(self.winner)
            return
        if bye == self.away_team:
            self.winner = self.home_team
            self.home_won = True
            #print "Bye for " + str(self.winner)
            return

        #print "Looking for %s v %s" % (self.home_team, self.away_team)
        for match in self.bracket.db_matches:
            home_team = frozenset([i for i in (match.home_player1, match.home_player2) if i])
            away_team = frozenset([i for i in (match.away_player1, match.away_player2) if i])
            teams = frozenset((home_team,away_team))
            wanted_teams = frozenset((self.home_team, self.away_team))
            if teams == wanted_teams:
                self.db_match = match
                h_sc, a_sc = match.get_score()
                if h_sc or a_sc:
                    h_score, a_score = [" ".join([str(i) for i in h_sc]),
                                          " ".join([str(i) for i in a_sc])]
                else:
                    h_score = a_score = ""

                if match.home_won != None:
                    #print "Got a result"
                    if match.home_won:
                        self.winner = home_team
                        win_score = h_score
                        lose_score = a_score
                    else:
                        self.winner = away_team
                        win_score = a_score
                        lose_score = h_score
                    if (self.winner == self.home_team):
                        self.home_won = True
                        self.home_score = (win_score or "1")
                        self.away_score = (lose_score or "0")
                    else:
                        self.home_won = False
                        self.home_score = (lose_score or "0")
                        self.away_score = (win_score or "1")
                break

    def isingle(self):
        yield self
    def __iter__(self):
        from itertools import chain
        c = chain(())
        if self.upper_match:
            c = chain(c, iter(self.upper_match))
        if self.lower_match:
            c = chain(c, iter(self.lower_match))
        return chain(c,self.isingle())

        

class Bracket(object):
    def __init__(self, league_id, playoff_league_id, cutoff=0):
        self.league_id = league_id
        self.playoff_league_id = playoff_league_id
        self.db_matches = M.Match.objects.filter(league_id=playoff_league_id)
        self.rounds, self.raw_bracket = playoff_list(self.league_id, cutoff)
        self.teams = flatten(self.raw_bracket)
        self.match_tree = self.make_match_tree(self.raw_bracket, self.rounds)
        self.lookup_matches(self.match_tree)

    def __str__(self):
        res = "L %d PL: %d\n" % (self.league_id, self.playoff_league_id)
        res += ""
        return res

    def get_json(self):
      data = {"teams": [], "results": [[]]}
      for match in self.match_tree:
          if match.round_num == 1:
              data["teams"].append(
                  [
                      {"name": " & ".join([(p.user.first_name+' ')[0] + ' ' + p.user.last_name
                                           for p in match.home_team ])},
                      {"name": " & ".join([(p.user.first_name+' ')[0] + ' ' + p.user.last_name
                                           for p in match.away_team ])},
                      ])

      for r in (1,2,3,4):
        results=[]
        matches = [m for m in self.match_tree if m.round_num == r]
        m_ix =0
        for match in matches:
          m_ix += 1
          comment = match_date = ""
          match_id = 0
          if match.db_match:
              match_id = match.db_match.id
              comment = match.db_match.comment
              match_date = str(match.db_match.date)
          hp1 = hp2 = ap1 = ap2 = 0
          if match.home_team:
              hps = list(match.home_team)
              hp1 = hps[0].id
              hp2 = hps[1].id if len(hps)> 1 else 0
          if match.away_team:
              aps = list(match.away_team)
              ap1 = aps[0].id
              ap2 = aps[1].id if len(aps)> 1 else 0

          results.append([match.home_score, match.away_score,
                          {"info": ("Comment: " + comment) if comment else "",
                           "id": match_id,
                           "date": match_date,
                           "league": self.playoff_league_id,
                           "round_num": int(match.round_num),
                           "match_num": m_ix,
                           "home_player1": hp1,
                           "home_player2": hp2,
                           "away_player1": ap1,
                           "away_player2": ap2,
                           }])
    
        if results:
            data["results"][0].append(results)
    
      return json.dumps(data)





    def lookup_matches(self, node):
        if node.upper_match:
            self.lookup_matches(node.upper_match)
        if node.lower_match:
            self.lookup_matches(node.lower_match)
        node.db_lookup()

    def make_match_tree(self, raw_bracket, round_num):
        # There better be two items corresponding to 
        # the two sides of the bracket node
        assert 2 == len(raw_bracket), "Bracket node doesn't have exactly two sides"
        home, away = raw_bracket
        match = Match(self, self.league_id, round_num)
        if list == type(home):
            match.upper_match = self.make_match_tree(home, round_num-1)
            match.lower_match = self.make_match_tree(away, round_num-1)
        else:
            # leaf level match
            match.home_team = home
            match.away_team = away

        return match

# matchups([1,2,3,4]) => [[1, 4], [2, 3]]
def matchups(teams):
    global bye
    if not bye:
        bye_list = M.Player.objects.filter(user__last_name='bye',user__first_name='')
        assert bye_list, "There is no player in database representing 'bye'. Need a user with last name bye and first name ''"
        bye = bye_list[0]
    n = len(teams)
    rounds = math.ceil(math.log(n)/math.log(2))
    N = int(2 ** rounds)
    res = []
    flip=False
    for ix in range(int(N/2)):
        t = teams[ix]
        opp_ix = N - 1 - ix
        if opp_ix > (n-1):
            opponent = frozenset((bye,))
        else:
            opponent = teams[opp_ix]
        if flip:
            res.append([opponent, t])
        else:
            res.append([t, opponent])
        flip = not flip
    if len(res) == 2:
        return res
    else:
        return matchups(res)

def flatten(l):
   res=[]
   for i in l:
       if type(i) == list:
           res=res+flatten(i)
       else:
           res.append(i)
   return res

injured_players = set() # ([M.Player.objects.get(user__last_name='Botsch')])

def playoff_list(league_id=1, cutoff=8, max_teams=8):
  league = M.League.objects.get(id=league_id)
  playoff_teams = [i[0]
                   for i in league.standings()[:max_teams]
                   if (i[1].points >= cutoff or i[1].wins >= 2) and not 
                   injured_players.intersection(i[0])]
  n = len(playoff_teams)
  rounds = math.ceil(math.log(n)/math.log(2))

  return rounds, matchups(playoff_teams)

