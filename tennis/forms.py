from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit, HTML, Button, Row, Field
from crispy_forms.bootstrap import AppendedText, PrependedText, FormActions

# optional stuff delete
from django.forms import inlineformset_factory
from .models import Player, Registration, EventRsvp

class RsvpForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(RsvpForm, self).__init__(*args, **kwargs)
        # If you pass FormHelper constructor a form instance
        # It builds a default layout with all its fields
        self.helper = FormHelper(self)

        # You can dynamically adjust your layout
        self.helper.layout.append(Submit('save', 'Save'))
        self.helper.layout.append( Submit('cancel', 'Cancel'))

    class Meta:
        model =EventRsvp
        exclude = ()

class PlayerForm(forms.ModelForm):
    singles = forms.BooleanField(required=False)
    doubles = forms.BooleanField(required=False)

    def __init__(self, *args, **kwargs):
        super(PlayerForm, self).__init__(*args, **kwargs)
        # If you pass FormHelper constructor a form instance
        # It builds a default layout with all its fields
        self.helper = FormHelper(self)

        # You can dynamically adjust your layout
        self.helper.layout.append(Submit('save', 'Save'))
        self.helper.layout.append( Submit('cancel', 'Cancel'))

    class Meta:
        model = Player
        fields = ['user','format_preference']


