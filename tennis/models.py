from __future__ import division
import re
import operator

from django.db import models
from django.contrib.auth.models import User, Group
from django import forms
from django.db.models.signals import post_save
from django.core.validators import MaxValueValidator, MinValueValidator

from django.utils import timezone as tz

from django.conf import settings
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

#convenience function
def xstr(s):
  return ('' if s is None else s)


class Player(models.Model):
    FORMAT_PREFERENCE_CHOICES = (
       ('S', "Singles"),
       ("D", "Doubles"),
    )
    user = models.OneToOneField(User, unique=True)
    mobile_phone = models.CharField(max_length=16, null=True, blank=True)
    work_phone = models.CharField(max_length=16, null=True, blank=True)
    home_phone = models.CharField(max_length=16, null=True, blank=True)

    format_preference = models.CharField(null=True, max_length=1, choices=FORMAT_PREFERENCE_CHOICES)
    social_user_id = models.CharField(max_length=32, null=True, blank=True)

    @property
    def name(self):
        return (u"%(fn)s %(ln)s" % {"fn": self.user.first_name, "ln": self.user.last_name})

    @property
    def short_name(self):
        return (u"%(fn)s %(ln)s" % {"fn": (self.user.first_name + " ")[0], "ln": self.user.last_name})

    def __str__(self):
        return str(self.user) or self.user.username

    def __lt__(self, other):
        return str(self).__lt__(str(other))

class PPrefs(models.Model):
    player = models.OneToOneField(Player, unique=True)
    p1 = models.ForeignKey(Player, related_name= "pref1", null=True, blank=True)
    p2 = models.ForeignKey(Player, related_name= "pref2", null=True, blank=True)
    p3 = models.ForeignKey(Player, related_name= "pref3", null=True, blank=True)
    rating_adj = models.DecimalField(max_digits=3, decimal_places=2, default=1.5,
                  validators=[MinValueValidator(0), MaxValueValidator(3.0)])
    def __str__(self):
        return self.player.__str__() + " prefs"


def BYE_PLAYER():
    return Player.objects.get(user__username="bye").id


# Automatically create a Player instance when a new user is created
def create_player(sender, instance, created, **kwargs):
    if created:
        Player.objects.create(user=instance)


post_save.connect(create_player, sender=User)

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)



# Organizations own leagues.
# Eg: Mercer Tennis Center, Hopewell Hackers,
class Organization(models.Model):
  name = models.CharField(max_length=64, unique=True)
  abbrev = models.CharField(max_length=16, blank=True, null=True)
  administrator = models.ForeignKey(Player)
  # This group should contain all people who belong to this organization
  members_group = models.ForeignKey(Group)
  def __str__(self):
    return xstr(self.abbrev)

# A League/Tournament
class League(models.Model):
  SEASON_CHOICES = (
        ('P', "Spring"),
        ('S', "Summer"),
        ('F', "Fall"),
        ('W', "Winter"),
    )

  LEAGUE_TYPE_CHOICES = (
    ('S', 'Singles'),
    ('D', 'Doubles'),
    ('X', 'Mixed Doubles'),
    )
  name = models.CharField(max_length=64, unique=True)
  abbrev = models.CharField(max_length=16, blank=True, null=True)
  org = models.ForeignKey(Organization)
  year = models.PositiveSmallIntegerField(null=True)
  season = models.CharField(null=True, max_length=1, choices=SEASON_CHOICES)
  division=models.CharField(null=True, max_length=32, help_text="MS, MD, WS, WD, XD, ...")
  group = models.CharField(null=True, max_length=1,
                           help_text='Group is one of A,B,C,...')
  begin_date = models.DateField('Begin Date', null=True)
#  league_type = models.CharField(null=True, max_length=2, choices=LEAGUE_TYPE_CHOICES)

  is_knockout = models.NullBooleanField(help_text="Is it a knockout/playoffs as opposed to round robin league?")
#  is_active  = models.NullBooleanField(help_text="Is this league still active?")


  def __str__(self):
    return xstr(self.abbrev)

  def standings(self, split_doubles=False):
      class Standing(object):
        def __init__(self, team):
          self.team = team
          self.points = 0
          self.sets_won = 0
          self.sets_lost = 0
          self.games_won = 0
          self.games_lost = 0
          self.games_played = 0
          self.wins = 0
          self.losses = 0
          self.avg_game_diff = 0

        def __hash__(self):
            return hash((self.team,))

        def __lt__(self, other):
            return ((other.points, other.avg_game_diff, self.sets_lost, self.games_lost) <
                    (self.points,  self.avg_game_diff,  other.sets_lost,other.games_lost))

        def __eq__(self, other):
            return (other.points == self.points and
                    other.avg_game_diff == self.avg_game_diff and
                    self.sets_lost == other.sets_lost and
                    self.games_lost == other.games_lost)

      d_standings = {}

      def get_teams(m):
          if m.home_player2:
              home= frozenset((m.home_player1, m.home_player2))
          else:
              home= frozenset((m.home_player1,))
          if m.away_player2:
              away= frozenset((m.away_player1, m.away_player2))
          else:
              away= frozenset((m.away_player1,))
          return home, away
 
 
      def update_standing(m, home, away):
          home_points, away_points, home_sets, away_sets, home_games, away_games = \
              m.get_result()
         
          if home not in d_standings:
            d_standings[home] = Standing(home)
          if away not in d_standings:
            d_standings[away] = Standing(away)

          h_st = d_standings[home]
          a_st = d_standings[away]

          h_st.points += home_points

          # Increment games played if this was not a default (i.e., at least 1 game played)
          game_played = 1 if (home_games > 0 or away_games > 0) else 0

          h_st.sets_won   += home_sets
          h_st.sets_lost   += away_sets
          h_st.games_won  += home_games
          h_st.games_lost  += away_games
  
          a_st.points += away_points
          a_st.sets_won += away_sets
          a_st.sets_lost += home_sets
          a_st.games_won += away_games
          a_st.games_lost += home_games
          if home_points != away_points:
              if home_points > away_points:
                  h_st.wins += 1
                  a_st.losses += 1
              else:
                  a_st.wins += 1
                  h_st.losses += 1

          if game_played:
               h_st.games_played += game_played
               a_st.games_played += game_played
               h_st.avg_game_diff = (h_st.games_won - h_st.games_lost)/h_st.games_played
               a_st.avg_game_diff = (a_st.games_won - a_st.games_lost)/a_st.games_played


      for m in self.match_set.all():
          home, away = get_teams(m)
          if split_doubles:
              for h, a in  zip(home,away):
                  update_standing(m, frozenset((h,)), frozenset((a,)))
          else:
              update_standing(m, home, away)
  
      return sorted(d_standings.items(), key=operator.itemgetter(1))

  def printable_standings(self, split_doubles=False):
      sorted_st = self.standings(split_doubles=split_doubles)
      def team_name(t):
          return " & ".join([p.short_name for p in t])

      return [{"player": team_name(t),
               "wins"  : st.wins,
               "losses": st.losses,
               "points": st.points,
               "avg_game_diff": ("%.2f" % st.avg_game_diff),
               "sets_lost"  : st.sets_lost,
               "games_lost" : st.games_lost}
              for (t, st) in sorted_st]

class Registration(models.Model):
  league = models.ForeignKey(League,
                             limit_choices_to={'year' : tz.now().date().year})
  player = models.ForeignKey(Player)
  def __str__(self):
    return xstr(self.player.__str__()) + ' ' + xstr(self.league.__str__())

  def clean(self):
    player_registrations = self.__class__.objects.filter(
      player_id=self.player_id,
      league__year=self.league.year,
      league__division=self.league.division)
    if player_registrations != None and len(player_registrations)> 0:
      pass
      # Commenting out. This validation is not expecially useful
      # Prevents legitimate registration for 18+ and 40+ leagues
      #raise forms.ValidationError("At most one registrion allowed per player for " +
      #                            self.league.division + " division")

# Typically a place with Tennis courts
class Facility(models.Model):
  name = models.CharField(max_length=64, unique=True)
  abbrev = models.CharField(max_length=16, blank=True, null=True)
  phone = models.CharField(max_length=16, null=True, blank=True)
  address = models.CharField(max_length=256, null=True, blank=True)
  map_url = models.URLField( blank=True)
  def __str__(self):
    return xstr(self.abbrev)

  class Meta:
    verbose_name_plural = "Facilities"

# A Facility has courts and each court has many time slots 
# identified by their start times 
class CourtTime(models.Model):
    location = models.ForeignKey(Facility)
    court_no = models.CharField(max_length=32)
    court_date = models.DateField()
    court_time = models.TimeField()
    reserved_by = models.ForeignKey(Player, null=True, blank=True, default=None)

    class Meta:
        unique_together = (("location", "court_no", "court_date", "court_time"),
                           ("reserved_by", "court_date"),
        )

    def __str__(self):
        return (self.court_no + " @ " + str(self.court_date) + " " +
                str(self.court_time) + " " + self.location.abbrev)

# A Tennis Match
class Match(models.Model):
  SCORE_RE = re.compile("^\s*(\d-\d) \s*(\d-\d)( \s*(\d-\d))?\s*$")
  SET_SCORE_RE = re.compile("^\s*(\d)-(\d)\s*$")
  date = models.DateField('Date of match')
  league = models.ForeignKey(League)
  round_num = models.PositiveSmallIntegerField(null=True)
  match_num = models.PositiveSmallIntegerField(null=True)
  home_player1 = models.ForeignKey(Player, related_name='hp1') 
  home_player2 = models.ForeignKey(Player, related_name='hp2', null=True, blank=True) 
  away_player1 = models.ForeignKey(Player, related_name='ap3') 
  away_player2 = models.ForeignKey(Player, related_name='ap4', null=True, blank=True) 
  home_won = models.NullBooleanField(null=True, blank=True)
  score = models.CharField(max_length=16, null=True, blank=True,
                           help_text="Example format: '4-6 6-4 1-0'. Two set win timed match:'6-2 4-2'")
  comment = models.CharField(max_length=256, blank=True)
  class Meta:
    unique_together = (("league", "round_num", "match_num"))

  @property
  def court(self):
    court=CourtTime.objects.filter(
      reserved_by__in=[
        self.home_player1,
        self.home_player2,
        self.away_player1,
        self.away_player2,
        ],
      court_date=self.date,
      )
    return court[0] if len(court) > 0 else None


  @property
  def name(self):
    return self.__str__()

  class Meta:
    verbose_name_plural = "Matches"
    unique_together = (#("date", "court"),
                       ("date", "home_player1", "away_player1"),
                       )

  def _parse_set_score(self, sc):
    if sc == None or sc == "": return 0,0, None
    m = self.SET_SCORE_RE.match(sc)
    home_games = int(m.group(1))
    away_games = int(m.group(2))
    return home_games, away_games, home_games > away_games

  def get_result(self):
    home_points, away_points, home_sets, away_sets, home_games, away_games, home_set_scores, away_set_scores = \
        self.get_result_full()
    return home_points, away_points, home_sets, away_sets, home_games, away_games
    
  def get_score(self):
    home_points, away_points, home_sets, away_sets, home_games, away_games, home_set_scores, away_set_scores = \
        self.get_result_full()
    return home_set_scores, away_set_scores
    
  def get_result_full(self):
    home_points = away_points = home_sets = away_sets = home_games = away_games = 0
    home_set_scores = []
    away_set_scores = []
    # If score has not been entered....
    if (self.score in [None,""]):
      if self.home_won == None:
        if self.away_player1.id == BYE_PLAYER():
          home_points =  3
        elif self.home_player1.id == BYE_PLAYER():
          away_points = 3

      else:
      # It may be a default
      # If one side has won, give them full points 
        if self.home_won:
          # No score set but there is a result. assume a double bagel
          home_points =  3
          home_sets   =  2
          home_games  = 12
        else:
          away_points =  3
          away_sets   =  2
          away_games  = 12
    elif self.home_won is None:
        # As of June 2015, we decided that 
        # home_won is None and score != "" => TIE GAME
        home_points = away_points = 2
        home_sets = away_sets = 0
        home_games = away_games = 0
    else:
      # Now deal with the case where a valid score is available
      m = self.SCORE_RE.match(self.score)
      assert m, "Invalid score in match"
      for set_no in (1,2,3):
        hg, ag, home_won_set = self._parse_set_score(m.group(set_no))
        if hg or ag:
            home_set_scores.append(hg)
            away_set_scores.append(ag)
        home_games = home_games + hg
        away_games = away_games + ag
        if home_won_set != None:
            if home_won_set:
                home_sets = home_sets + 1
            else:
                away_sets = away_sets + 1
        home_points = 1 + home_sets
        away_points = 1 + away_sets


    if self.home_won != None:
        if self.home_won != (home_points > away_points):
            # Score must have been entered flipped
            # Reverse everything
            home_points, away_points = away_points, home_points
            home_sets, away_sets = away_sets, home_sets
            home_games, away_games = away_games, home_games
            home_set_scores, away_set_scores = away_set_scores, home_set_scores
    return home_points, away_points, home_sets, away_sets, home_games, away_games, home_set_scores, away_set_scores


  @property
  def home_points(self):
      return self._parse_score()[0];


  @property
  def away_points(self):
      return self._parse_score()[1];


  def clean(self):
    if not (self.score in [None, ""]):
        m = self.SCORE_RE.match(self.score)
        if m == None:
            raise forms.ValidationError(self.score + " is not a valid score")
        # New rule 6/1/2015
        # non blank score with hom_won==None => tie
        # Do not prevent it.
        #if self.home_won is None:
        #    raise forms.ValidationError("Score is set but 'Home Won?' is not")


            # if self.court != None:
        #      instance_exists = Match.objects.filter(date=self.date,
        #                                             court=self.court).exclude(id=self.id).exists()
        #      if instance_exists:
        #        raise forms.ValidationError(self.court.__str__() + \
        #                                      'is already taken')


  def home_name(self):
    name = ""
    if self.home_player1:
        name = name + self.home_player1.short_name
    if self.home_player2:
        name = name + " & " + self.home_player2.short_name
    return name


  def away_name(self):
    name = ""
    if self.away_player1:
        name = name + self.away_player1.short_name
    if self.away_player2:
        name = name + " & " + self.away_player2.short_name
    return name

  def __str__(self):
      return xstr(self.home_name()) + " vs " + xstr(self.away_name())

  @property
  def winner(self):
    name = ""
    if self.home_won == None:
        pass
    elif self.home_won:
        name = self.home_name()
    else:
        name = self.away_name()
    return name

class Event(models.Model):
  EVENT_TYPE_CHOICES = (
        ('HL', "Hackers League"),
	('U4', 'USTA 4.0 18+ Match'),
	('u4', 'USTA 4.0 40+ Match'),
	('U3', 'USTA 3.5 18+ Match'),
	('u3', 'USTA 3.5 40+ Match'),
        ('GT', 'Get Together'),
        ('OT', 'Other'),
  )
  event_type = models.CharField(null=True, max_length=2, choices=EVENT_TYPE_CHOICES)
  date = models.DateTimeField('Event date')
  name = models.CharField(max_length=64)
  comment = models.CharField(max_length=256, blank=True, null=True)
  org = models.ForeignKey(Organization)
  league = models.ForeignKey(League, null=True, blank=True)
  always_show = models.BooleanField(default=False, help_text='Display this Event in home page even if it is in distant future')
  class Meta:
    unique_together = ("date", "name", "org")
  def __str__(self):
    t = self.get_event_type_display()
    return xstr((t or "") + " -- " + self.name )

class EventRsvp(models.Model):
  event = models.ForeignKey(Event)
  player = models.ForeignKey(Player)
  RESPONSE_CHOICES = (
        ('A', "Available"),
        ('1', "At most 1/weekend"),
        ('I', "If Needed"),
        ('N', "Not Available"),
  )
  _RESP = dict(RESPONSE_CHOICES)
  response = models.CharField(max_length=1, choices=RESPONSE_CHOICES, default='A')
  comment = models.CharField(max_length=256, blank = True)

  class Meta:
    unique_together = ("event", "player")
  def __str__(self):
    return self.player.__str__() + ' Responded ' + \
        xstr(self._RESP[self.response]) + ' for ' + self.event.__str__()
