from django.forms import widgets
from rest_framework import serializers
#from tennis.models import Facili, LANGUAGE_CHOICES, STYLE_CHOICES
import tennis.models as models

class FacilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Facility
        fields = ('id', 'abbrev', 'name', 'map_url')
