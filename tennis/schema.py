import graphene as G
import graphene_django as GD
from graphene_django.filter import DjangoFilterConnectionField

from . import models as M


# Graphene will automatically map the Category model's fields onto the CategoryNode.
# This is configured in the CategoryNode's Meta class (as you can see below)
class FacilityNode(GD.DjangoObjectType):
    class Meta:
        model = M.Facility
        filter_fields = ['name', 'abbrev']
        interfaces = (G.relay.Node, )


class PlayerNode(GD.DjangoObjectType):
    class Meta:
        model = M.Player
        # Allow for some more advanced filtering here
        filter_fields = {
             'mobile_phone': ['exact', 'icontains', 'istartswith'],
        #     'notes': ['exact', 'icontains'],
        #     'category': ['exact'],
        #     'category__name': ['exact'],
        }
        interfaces = (G.relay.Node, )


class Query_(G.AbstractType):
    facility = G.relay.Node.Field(FacilityNode)
    all_facilities = DjangoFilterConnectionField(FacilityNode)

    player = G.relay.Node.Field(PlayerNode)
    all_players = DjangoFilterConnectionField(PlayerNode)

class Query(Query_, G.ObjectType):
    # This class will inherit from multiple Queries
    # as we begin to add more apps to our project
    pass

schema = G.Schema(query=Query)
