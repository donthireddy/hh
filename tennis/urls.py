from django.conf.urls import include, url
#from django.views.generic import TemplateView      
#from tennis.views import PersonListView
from django.views.generic import DetailView, ListView
from . import views, models, admin, schema
from django.views.generic import TemplateView
from django.contrib.auth.views import logout
import graphene_django.views as GDV
import hh_auth0

class SimpleStaticView(TemplateView):
    def get_template_names(self):
        return [self.kwargs.get('template_name') + ".html"]
    
    def get(self, request, *args, **kwargs):
        from django.contrib.auth import authenticate, login
        if request.user.is_anonymous():
            # Auto-login the User for Demonstration Purposes
            user = authenticate()
            login(request, user)
        return super(SimpleStaticView, self).get(request, *args, **kwargs)

urlpatterns = [
    url(r'^', include(admin.tennis_admin_site.urls)),
    url(r"^bracket/(?P<playoff_league_id>\d+)$", views.bracket),
    url(r"^br/(?P<br_id>\d+)$", views.bracket_ui, name="bracket"),
    url(r"^players/(?P<group_id>\d+)$", views.players),

    url(r"^auth0/$",  views.auth, name="auth0_login"),
    url(r"^auth0/(?P<callback>.*)$",  views.auth, name="auth0_login"),
    url(r"^logout/(?P<next_page>.*)$",  logout, name="app_logout"),
    url(r"^auth/",  include('hh_auth0.urls')), #hh_auth0.views),
    url(r"^registrations$",  views.registrations),
    url(r"^player$",  views.player),
    url(r"^matches/(?P<league_id>\d+)$", views.matches),
    url(r"^my_matches/(?P<userid>\d+)$", views.my_matches),
    url(r"^standings/(?P<league_id>\d+)/(?P<split_doubles>\d+)", views.standings),
    url(r"^events/(?P<org_id>\d+)/", views.events),
    url(r"^upcoming_events/(?P<org_id>\d+)/", views.upcoming_events),
    url(r"^rsvps/(?P<event_id>\d+)$", views.rsvps),
    url(r"^sched/", views.scheduling_data),
    url(r"^get_lineups/", views.get_lineups),
    url(r"^create_matches/", views.create_matches),

    # For form POST
    url(r'^(?P<template_name>\w+)$', SimpleStaticView.as_view(), name='tennis'),
    url(r'^player/edit/(?P<user_id>\d+)$', views.player_update, name='player_edit'),
    url(r'^rsvp/edit/(?P<event_id>\d+)/(?P<user_id>\d+)$', views.rsvp_update, name='rsvp_edit'),
    url(r'^graphql', GDV.GraphQLView.as_view(graphiql=True, schema=schema.schema)),
]

