from tastypie.authorization import Authorization,DjangoAuthorization
from tastypie.resources import ModelResource
from tastypie import fields
from tennis.models import Organization, League
from tastypie.authentication import BasicAuthentication, SessionAuthentication, ApiKeyAuthentication,MultiAuthentication

class OrganizationResource(ModelResource):
     class Meta:
        queryset = Organization.objects.all()
        authorization= Authorization()
        #resource_name = 'organization'
        authentication = SessionAuthentication()

class LeagueResource(ModelResource):
    org = fields.ForeignKey(OrganizationResource, "org", related_name='org_id')
    class Meta:
        queryset = League.objects.all()
        #resource_name = 'league' # league is default anyway
        authorization= Authorization()
        authentication = SessionAuthentication()
