from django.contrib import admin
from tennis.models import (Player, PPrefs, Organization, League, Match, CourtTime,
                           Facility, Event, EventRsvp, Registration)

from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.admin.sites import AdminSite
from import_export import resources
from import_export.admin import ImportExportModelAdmin

class PlayerNameEmail(User):
    class Meta:
        proxy = True

class PlayerPhone(Player):
    class Meta:
        proxy = True

class PlayerInline(admin.StackedInline):
    model = Player
    max_num = 1
    can_delete = False
    verbose_name_plural = 'player'

class UserAdmin(AuthUserAdmin):
    inlines = (PlayerInline,)

class PlayerNameEmailAdmin(AuthUserAdmin):
  list_display =("username", "first_name", "last_name", "email",)
                 #"mobile_phone")
  inlines = [PlayerInline]
  list_display_link=("username",)
  #list_editable = list_display[1:]
  list_filter = ("username", "first_name", "last_name", "email",)
  ordering = ("last_name","first_name")
  search_fields=('first_name',
                 'last_name',
                 'username',
                 'email',
                   )
  def mobile_phone(self, obj): return obj.player.mobile_phone


class PlayerAdmin(admin.ModelAdmin):
    list_display="user name mobile_phone work_phone home_phone".split(" ")
    list_display_link=("user",)
    #list_editable = "mobile_phone work_phone home_phone".split(" ")
    list_filter = ("user", "user__first_name", "user__last_name")
    ordering = ("user",)
    search_fields=('user__first_name',
                   'user__last_name',
                   'user__username',
                   )

class PPrefsAdmin(admin.ModelAdmin):
    list_display=('player', 'p1', 'p2','p3', 'rating_adj')
    list_filter = ('player', 'p1', 'p2','p3')
    ordering = ("player",)

class EventRsvpAdmin(admin.ModelAdmin):
    list_display=('player', 'event', 'response', 'comment')
    list_filter = "event player response".split(" ")
    ordering = ("event", "player")
 
class MatchAdmin(admin.ModelAdmin):
    list_display=('name', 'date', 'score', 'league', 'court')
    #list_display_link=("name",)
    #list_editable = "date score".split(" ")
    list_filter = "league date".split(" ")
    ordering = "league date".split(" ")
    #search_fields= "away_player1 away_player2 comment home_player1 home_player2".split(" ")
    search_fields= [
        "away_player1__user__first_name",
        "away_player1__user__last_name",
        "away_player2__user__first_name",
        "away_player2__user__last_name",
        "home_player1__user__first_name",
        "home_player1__user__last_name",
        "home_player2__user__first_name",
        "home_player2__user__last_name",
        "comment"]

class CourtTimeAdmin(admin.ModelAdmin):
    list_display=('court_date', 'court_time', 'reserved_by', 'location', 'court_no',
                   'id')
    list_display_links=('id',)
    #list_editable = ('reserved_by',)
    list_filter = ('location','reserved_by')
    ordering = ('location', 'court_date', 'court_time', 'court_no',)
    lines_per_page=25
    search_fields=('reserved_by__user__first_name',
                   'reserved_by__user__last_name',
                   'reserved_by__user__username',
                   'location')

class FacilityAdmin(admin.ModelAdmin):
    list_display=('abbrev', 'name',
                  'id')
    list_display_links=('id',)
    list_editable = list_display[:-1]
    #list_filter = ('location','reserved_by')
    ordering = ('abbrev',)
    #lines_per_page=25

class RegAdmin(admin.ModelAdmin):
    list_display="league player id".split(" ")
    list_display_links=('id',)
    #list_editable = list_display[:-1]
    list_filter = "league__year player league".split(" ")
    ordering = "league__year league player".split(" ")

class RegistrationResource(resources.ModelResource):
    class Meta:
        model = Registration

class RegistrationAdmin(ImportExportModelAdmin):
    resource_class = RegistrationResource
    pass

class LeagueAdmin(admin.ModelAdmin):
    list_display="name abbrev year season division group org id".split(" ")
    list_display_links=('id',)
    #list_editable = list_display[:-1]
    list_filter = "year org".split(" ")
    ordering = "year name".split(" ")

# unregister old user admin
admin.site.unregister(User)
# register new user admin
admin.site.register(User, UserAdmin)
admin.site.register(PlayerNameEmail, PlayerNameEmailAdmin)

admin.site.register(PlayerPhone, PlayerAdmin)
admin.site.register(Player)
admin.site.register(PPrefs, PPrefsAdmin)
admin.site.register(Organization)
admin.site.register(League, LeagueAdmin)
admin.site.register(Match, MatchAdmin)
admin.site.register(CourtTime, CourtTimeAdmin)
admin.site.register(Facility, FacilityAdmin)
admin.site.register(Event)
admin.site.register(EventRsvp, EventRsvpAdmin)
admin.site.register(Registration, RegAdmin)

class TennisAdmin(AdminSite):
    def has_permission(self, request):
        """
        Removed check for is_staff.
        """
        return True # request.user.is_active
 
tennis_admin_site = TennisAdmin(name='tennisadmin')

tennis_admin_site.register(Event)
tennis_admin_site.register(EventRsvp, EventRsvpAdmin)
