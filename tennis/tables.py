from django.utils.safestring import mark_safe
from django.utils.html import escape
import django_tables2 as tables
from . import models
import re

class EventTable(tables.Table):
    id = tables.Column()
    date = tables.Column()
    event = tables.Column()
    comment = tables.Column()
    rsvp = tables.Column()
    see_rsvps = tables.Column(accessor="id")

    def render_id(self, value):
        return mark_safe("<a target=\"_parent\" href=/admin/tennis/event/%(key)s/>%(key)s </a>" %
                {"key": escape(value)}) if value else ""

    def render_rsvp(self, value):
        m = re.match("^(\d+)\/(\d+)$", value)
        rsvp="RSVP"
        if m:
           event_id, user_id = int(m.group(1)), int(m.group(2))
           rsvps = models.EventRsvp.objects.filter(event_id=event_id, player__user_id=user_id)
           if rsvps:
               rsvp = rsvps[0].get_response_display()
           
        return mark_safe("<a target=\"_parent\" href=/tennis/rsvp/edit/%(key)s>%(rsvp)s </a>" %
                {"key": escape(value), "rsvp": rsvp}) if value else ""
    def render_see_rsvps(self, value):
        return mark_safe("<a target=\"_parent\" href=/tennis/rsvps/%(event_id)s> See RSVPs </a>" %
                {"event_id": escape(value)})
    class Meta:
        attrs = {"class": "paleblue"}

class RsvpTable(tables.Table):
    date = tables.Column()
    event = tables.Column()
    player = tables.Column()
    response = tables.Column()
    comment  = tables.Column()

    class Meta:
        attrs = {"class": "paleblue"}

class RegistrationsTable(tables.Table):
    name = tables.Column()
    preference = tables.Column()
    singles = tables.BooleanColumn()
    doubles = tables.BooleanColumn()
    class Meta:
        attrs = {"class": "paleblue"}

class MatchUpdateLinkColumn(tables.Column):
    def render(self, value):
        return mark_safe("<a href=/admin/tennis/match/%(id)s> %(id)s </a>" %
                {"id": escape(value)})

class PlayerUpdateLinkColumn(tables.Column):
    def render(self, value):
        return mark_safe("<a href=/admin/auth/user/?player__id=%(id)s> %(id)s </a>" %
                {"id": escape(value)})

class PlayerTable(tables.Table):
    name = tables.Column(order_by=("user__first_name", "user__last_name"))
    email = tables.EmailColumn(accessor='user.email', verbose_name="email")
    id = PlayerUpdateLinkColumn()
    class Meta:
        model = models.Player
        sequence = ("id", "user","name","email","mobile_phone","work_phone",
                    "home_phone")
        # add class="paleblue" to <table> tag
        attrs = {"class": "paleblue"}

class MatchTable(tables.Table):
    name = tables.Column(verbose_name="name",order_by=("home_player1","away_player1"))
    winner = tables.Column(orderable=False)
    court = tables.Column(orderable=False)
    id = MatchUpdateLinkColumn()
    league_abb = tables.Column(accessor="league.abbrev", verbose_name="league_abb")
    class Meta:
        model = models.Match
        sequence = ("id", "date","name","court","league_abb", "winner","score","comment")
        exclude = ("home_player1","home_player2","away_player1",
                   "away_player2","league","home_won")
        # add class="paleblue" to <table> tag
        attrs = {"class": "paleblue"}

class StandingsTable(tables.Table):
    player = tables.Column()
    tmpl_right_align = '<div align="right">{{ value }}</div>'
    # Alignment not working
    wins   = tables.TemplateColumn(template_code = tmpl_right_align)
    losses = tables.TemplateColumn(template_code = tmpl_right_align)
    points = tables.TemplateColumn(template_code = '<div align="right"><strong>{{ value }}</strong></div>') #
    avg_game_diff   = tables.TemplateColumn(template_code = tmpl_right_align)
    sets_lost   = tables.TemplateColumn(template_code = tmpl_right_align)
    games_lost  = tables.TemplateColumn(template_code = tmpl_right_align)
    class Meta:
        attrs = {"class": "paleblue"}
        # sequence = ("points","sets","games")


