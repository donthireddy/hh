from django.shortcuts import render, redirect, render_to_response, get_object_or_404
from django_tables2   import RequestConfig, Table, Column
from . import models, tables, models as M
import util.matches
Facility= models.Facility
from . import forms
from django.utils.decorators import method_decorator

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from .serializers import FacilitySerializer
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from .bracket_util import  *
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.utils import timezone as tz
from django.views.generic import CreateView
from django.core.urlresolvers import reverse_lazy
from django.template import RequestContext
from collections import defaultdict
import get_scheduling_data as sched
from optimal_hh_matchups import do_it as schedule
import json
from django.http import StreamingHttpResponse
from django.views.decorators.csrf import csrf_exempt
from dotmap import DotMap

def auth(request, **kw_args):
    return render(request, "auth.html", kw_args)

@csrf_exempt
def create_matches(request):
    if request.method=='POST':
        received_json_data=json.loads(request.body.decode("utf-8"))
        res = util.matches.create_matches(received_json_data)
        return StreamingHttpResponse(json.dumps(res))
    return StreamingHttpResponse('it was GET request')

@csrf_exempt
def get_lineups(request):
    if request.method=='POST':
        received_json_data=json.loads(request.body.decode("utf-8"))
        if "magic" in received_json_data:
            received_json_data=sched.load_data()
        singles,doubles = schedule(DotMap(received_json_data))
        return StreamingHttpResponse(json.dumps({"Singles":singles,"Doubles":doubles }))
    return StreamingHttpResponse('it was GET request')


def scheduling_data(request):
    jsonp_callback = request.GET.get("callback")
    data = sched.load_data()
    #data={"foo":23,"bar":[23,33,91]}
    if jsonp_callback:
        response = HttpResponse("%s(%s);" % (jsonp_callback, json.dumps(data, indent=4)))
        response["Content-type"] = "text/javascript; charset=utf-8"
    else:
        response = HttpResponse(json.dumps(data, indent=4))
        response["Content-type"] = "application/json; charset=utf-8"
    return response

def _events(request, org_id, events, leagues):
    data = [ {"date": e.date, "event": e.__str__(), "id": e.id,
              "rsvp" : ("%d/%d" % (e.id, request.user.id))
                  if request.user.id and e.date.date() >= tz.now().date()
                     and (e.league_id is None or e.league_id in leagues)
                  else None,
              "comment" : e.comment,
              }
             for e in events]
    table = tables.EventTable(data, order_by=("date",))
    RequestConfig(request, paginate={"per_page": 50}).configure(table)
    return render(request, "events_table.html", {"table" : table})


def events(request, org_id):
    org_id=int(org_id)
    user_id = int(request.user.id)
    leagues = [r.league.id for r in M.Registration.objects.filter(player__user__id=user_id)]
    event_list = M.Event.objects.filter(org__id=org_id, date__year=tz.now().date().year)
    return _events(request, org_id, event_list, leagues)

def upcoming_events(request, org_id):
    max_rows=int(request.GET.get('max_rows',6))
    always_show= (0 != int(request.GET.get('always_show',0)))
    org_id=int(org_id)
    user_id = int(request.GET.get('user_id', 0))
    leagues = [r.league.id for r in M.Registration.objects.filter(player__user__id=user_id)]
    event_list = M.Event.objects.filter(
        Q(org__id=org_id),
        Q(always_show=always_show),
        Q(date__gte=tz.now().date()),
        Q(league__isnull=True) | Q(league_id__in=leagues),
      ).order_by('-always_show', 'date')[:max_rows]
    return _events(request, org_id, event_list, leagues)


def rsvps(request, event_id):
    rsvps = M.EventRsvp.objects.filter(event__id=event_id)
    data = [ {"date": r.event.date, "event": r.event.__str__(),
              "player": r.player.__str__(), "response": r.get_response_display(),
              "comment": r.comment
             }
             for r in rsvps]
    table = tables.RsvpTable(data)
    RequestConfig(request, paginate={"per_page": 50}).configure(table)
    return render(request, "rsvps.html", {"table" : table})


def registrations(request):
    reg_dict = defaultdict(bool)
    players = {}
    regs = M.Registration.objects.filter(league__year=tz.now().date().year) 
    for reg in regs:
        players[reg.player.name] = reg.player.get_format_preference_display()
        if reg.league.division == 'MS':
            reg_dict[reg.player.name,'Singles'] = True
        if reg.league.division == 'MD':
            reg_dict[reg.player.name,'Doubles'] = True
    data = [{"name": p, "singles": reg_dict[p, 'Singles'],
                        "doubles": reg_dict[p, 'Doubles'],
              "preference": players[p]} for p in sorted(players.keys())]

    table = tables.RegistrationsTable(data)
    RequestConfig(request, paginate={"per_page": 50}).configure(table)
    return render(request, "registrations.html", {"table" : table})

def rsvp_update(request, event_id, user_id, template_name='rsvp_form.html'):
    rsvp = M.EventRsvp.objects.filter(event_id=event_id, player__user__id=user_id).first()
    if not rsvp:
        rsvp = M.EventRsvp()
        rsvp.event = get_object_or_404(M.Event, id=event_id)
        rsvp.player = get_object_or_404(M.Player, user__id=user_id)

    form = forms.RsvpForm(request.POST or None, instance=rsvp)
    if form.is_valid():
        rsvp = form.save()
        return redirect('home')
    return render(request, template_name, {'form':form})

def player_update(request, user_id, template_name='player_form.html'):
    player = get_object_or_404(M.Player, user__id=user_id)
    regs = M.Registration.objects.filter(player__id=player.id, league__year=tz.now().date().year)
    doubles = any([r.league.division == 'MD' for r in regs])
    singles = any([r.league.division == 'MS' for r in regs])
    form = forms.PlayerForm(request.POST or None, instance=player,
                            initial = {'doubles': doubles, 'singles':singles})
    if form.is_valid():
        player = form.save()
        regs = M.Registration.objects.filter(player__id=player.id, league__year=tz.now().date().year)
        singles, doubles = False, False
        for r in regs:
            if r.league.division == 'MS':
              singles = True
              if not form.cleaned_data['singles']:
                r.delete()
            if r.league.division == 'MD':
              doubles = True
              if not form.cleaned_data['doubles']:
                r.delete()
        if form.cleaned_data['singles'] and not singles:
            singles_league = M.League.objects.get(year=tz.now().date().year, division='MS')
            M.Registration(league=singles_league, player=player).save()
        if form.cleaned_data['doubles'] and not doubles:
            doubles_league = M.League.objects.get(year=tz.now().date().year, division='MD')
            M.Registration(league=doubles_league, player=player).save()

        return redirect('home')
    return render(request, template_name, {'form':form})


def bracket(request, playoff_league_id):
  playoff_league_id = int(playoff_league_id)
  if playoff_league_id == 3:
    cutoff = 8
    league_id = 1
  elif playoff_league_id == 11:
    cutoff = 0
    league_id = 5
  elif playoff_league_id == 12:
    cutoff = 0
    league_id = 6
  elif playoff_league_id == 13:
    cutoff = 8
    league_id = 7
  elif playoff_league_id == 14:
    cutoff = 0
    league_id = 8
  elif playoff_league_id == 19:
    cutoff = 0
    league_id = 18
  else:
    cutoff = 0
    league_id = 2

  return HttpResponse(Bracket(league_id, playoff_league_id,cutoff).get_json(),
                      content_type='application/json')
#                      mimetype='application/json')

def bracket_ui(request, **kwargs):
    return render(request, 'bracket.html', kwargs)
    

@login_required
def players(request, group_id):
    table = tables.PlayerTable(
         models.Player.objects.filter(user__groups__id=group_id).order_by('-user__is_active','user__username'))
    RequestConfig(request, paginate={"per_page": 50}).configure(table)
    return render(request, 'players.html', {'table': table})

@login_required
def matches(request, league_id=1):
    table = tables.MatchTable(
         models.Match.objects.filter(league_id=league_id).order_by('date', 'home_player1'))
    RequestConfig(request, paginate={"per_page": 50}).configure(table)
    return render(request, 'matches.html', {'table': table})

@login_required
def my_matches(request, userid=1):
    
    table = tables.MatchTable(
         models.Match.objects.filter(
         Q(home_player1__user__id=userid) |
         Q(home_player2__user__id=userid) |
         Q(away_player1__user__id=userid) |
         Q(away_player2__user__id=userid),
         Q(date__year=tz.now().date().year)
                                     ).order_by('date', 'home_player1'))
    RequestConfig(request, paginate={"per_page": 50}).configure(table)
    return render(request, 'matches.html', {'table': table})

@login_required
def player(request):
    return render(request, 'player.html', {'form': forms.PlayerForm()})

@login_required
def standings(request, league_id=1, split_doubles=0):
    league = models.League.objects.get(id=int(league_id))
    table=tables.StandingsTable(league.printable_standings(split_doubles=bool(split_doubles)))
    return render(request, 'standings.html', {'table': table})

#@render_to('homepage.html')
#def home(request):
#  request.user.
#  return {
#    "player":player
#    }

class FacilityListView(APIView):
    queryset = Facility.objects.all()

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(FacilityListView, self).dispatch(*args, **kwargs)

    def get(self, request, format=None):
        facilitys = self.queryset
        serializer = FacilitySerializer(facilitys, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):     
        serializer = FacilitySerializer(data=request.DATA)
        if serializer.is_valid():
          serializer.save()
          return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
          return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class FacilityDetailView(APIView):
    queryset = Facility.objects.all()

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(FacilityDetailView, self).dispatch(*args, **kwargs)

    def get_object(self, pk):
      try:
        return self.queryset.get(pk=pk)
      except Facility.DoesNotExist:
        raise Http404

    def get(self, request, pk, format=None):
        facility = self.get_object(pk)
        serializer = FacilitySerializer(facility)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        facility = self.get_object(pk)
        serializer = FacilitySerializer(facility, data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        facility = self.get_object(pk)
        facility.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
