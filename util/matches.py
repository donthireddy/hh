#!/usr/bin/env python3

import re
import tennis.models as m
import smtplib
from django.db import IntegrityError
import datetime
import json

def send_email(tolist, message):
    user = 'maverickone@gmail.com'
    password = mypassword
    smtpserv="smtp.gmail.com:587"
    mailserver=smtplib.SMTP(smtpserv)
    mailserver.starttls()
    mailserver.login(user,password)
    mailserver.sendmail(user,tolist,message)

# %cpaste
def add_courttime(loc, num, date, time):
    loc = m.Facility.objects.get(abbrev = loc)
    m.CourtTime(location=loc, court_no=num, court_date=date,
                court_time=time).save()
    

def add_smatch(l, h1, a1, date, round_num, match_num):
    league_args = {"id":int(l)} if isinstance(l, int) or re.match("^\d+$", l) else {"abbrev": l}
    singles = m.League.objects.get(**league_args)
    h1 = m.Player.objects.get(user__username=h1)
    a1 = m.Player.objects.get(user__username=a1)
    match = m.Match(league=singles,
            home_player1=h1,
            away_player1=a1,
            date=date,
            round_num=round_num,
            match_num=match_num)
    match.save()
    return match 

def add_dmatch(l, h1, h2, a1, a2, date,round_num, match_num):
    league_args = {"id":int(l)} if isinstance(l, int) or re.match("^\d+$", l) else {"abbrev": l}
    doubles = m.League.objects.get(**league_args)
    h1 = m.Player.objects.get(user__username=h1)
    h2 = m.Player.objects.get(user__username=h2)
    a1 = m.Player.objects.get(user__username=a1)
    a2 = m.Player.objects.get(user__username=a2)
    match = m.Match(league=doubles,
            home_player1=h1,
            home_player2=h2,
            away_player1=a1,
            away_player2=a2,
            date=date,
            round_num=round_num,
            match_num=match_num)
    match.save()
    return match

def delete_matches(l_abbrev, date):
    m.Match.objects.filter(league__abbrev=l_abbrev, date=date).delete()

def get_last_round_num(l_abbrev, date):
    return m.Match.objects.filter(league__abbrev=l_abbrev, date__lt=date).latest('date').round_num 

def create_matches(data=None, date=None, clean_first=True):
  """
    data= {
       "Doubles" : [
          [['h1','h2'],['a1','a2']],
           ...
           
       ]
       "Singles" : [['h', 'a' ], ...]
    }
  """
  matches=[]
  if data is None:
      data = {
      }
  league_abbr ={'Doubles': "HH17D", "Singles": "HH17S"}
  if date is None:
    date=(datetime.date.today() + datetime.timedelta(days=1)).strftime('%Y-%m-%d')

  for l in data.keys():
       l_abbrev = league_abbr.get(l,l)
       if clean_first:
          delete_matches(l_abbrev, date)
       round_num = get_last_round_num(l_abbrev, date) + 1
       match_num = 0
       match = None
       for m in data[l]:
           match_num += 1 
           if len(m) != 2:
              continue
           try:
             if isinstance(m[0], str):
               # singles
               match = add_smatch(l_abbrev,m[0], m[1], date, round_num, match_num)
             else:
               match = add_dmatch(l_abbrev, *m[0], *m[1], date, round_num, match_num)
           except IntegrityError as e:
               print("Failed on "+str(m))
           print(match, type(match.date))
           matches.append({"match_id": match.id, "date": match.date, "round": match.round_num, "match": match.match_num,
                       "players": str(match)})
  return matches
