import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mcp.settings")
from django.core.mail import EmailMessage
import tennis.models as M
import django
import mcp.settings as S

# 
django.setup()

SENDER="%s@%s" % (S.EMAIL_HOST_USER, S.EMAIL_HOST)

def send_mail(subject, body, to):
    # XXX
    bcc=[]
    reply_to=['donthireddy@yahoo.com']
    print ("Sending email to "+str(to))
    headers={'Message-ID': 'HH_email'}
    email = EmailMessage('[HH] '+subject, body, SENDER, 
            to, bcc, reply_to=reply_to,
                     headers=headers)
    email.content_subtype="html"
    email.send(fail_silently=False)

