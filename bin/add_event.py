#!/usr/bin/env python3
import os, sys
os.environ['DJANGO_SETTINGS_MODULE'] = 'mcp.settings'
import django
django.setup()
#from django.conf import settings
#from django.core.wsgi import get_wsgi_application
#application = get_wsgi_application()
import tennis.models as M
import pytz
import argparse
from datetime import datetime 

tz=pytz.timezone('America/New_York')

HH = M.Organization.objects.get(id=1)

def valid_datetime(s):
    try:
        return datetime.strptime(s, "%Y-%m-%d %H:%M")
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)
parser = argparse.ArgumentParser(description="Create new Hackers Event");
parser.add_argument('--event_type', type=str)
parser.add_argument(
    '--datetime',
    help="The Start Date and Time - format YYYY-MM-DD HH:MM",
                    required=True,
                    type=valid_datetime)
parser.add_argument('--name', type=str, required=True)
parser.add_argument('--league', type=str, required=False)
parser.add_argument('--comment', type=str)
parser.add_argument('--always_show', dest='always_show', action='store_true')
parser.add_argument('--no-always_show', dest='always_show', action='store_false')
parser.set_defaults(always_show=False)
args = parser.parse_args()
if args.league: 
    league = M.League.objects.get(abbrev=args.league)
else:
    league = None
event =M.Event(
    event_type=args.event_type,
    date=tz.localize(args.datetime),
    name=args.name,
    comment=args.comment,
    always_show=args.always_show,
    org=HH,
    league=league)

print("Save returned: ", str(event.save()))
print ("ID? ", event.id)

# XXX TODO Registrations
