#!/usr/bin/env python3
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'mcp.settings'
import django
django.setup()

from util.mail import send_mail
import tennis.models as M

members = M.Organization.objects.get(id=1).members_group.user_set.filter(
    is_active=True).order_by('email')
recipients = [p.email for p in members]
subject = "Reminder to sign up for upcoming games"
body="""
    <p>
    Reminder to visit the <a href="http://hh.mariandrive.com/">website</a>,
    login and RSVP to upcoming games if you haven\'t already</p>
    """

print("Sending email to %d members" % len(recipients))
send_mail(subject, body, to=recipients)

