#!/usr/bin/env python
# REST API client library
#
import requests
import json
#import argparse

ROOT_URL='http://hh.donthireddy.us/api'
#murali token
TOKEN = '8ce539373bcd2438df5f534fdf57689ba848d83f'

#parser = argparse.ArgumentParser(description="API Getter");
#parser.add_argument('url', metavar='url', type=str, nargs=1,
#                    help='urls bit that gets appended to '+ROOT_URL)
#args = parser.parse_args()
#url =args.url[0]


def api_op(op,*args, **kw_args):
  if 'token' in kw_args:
     token=kw_args.pop('token')
  else:
    token = TOKEN 

  return op(*args, **kw_args, headers={'Authorization': 'Token '+token})


def get_list(collection='facilities', token=TOKEN):
  url = "/".join([ROOT_URL, collection, ""])  
  return api_op(requests.get, url, token=token)


def get_obj(collection='facilities', id=10, token=TOKEN):
  url = "/".join([ROOT_URL, collection, str(id), ""])
  return api_op(requests.get, url, token=token)


def update_obj(collection='facilities', id=10, data={}, token=TOKEN):
  url = "/".join([ROOT_URL, collection, str(id), ""])
  return api_op(requests.put, url, data=data, token=token)


def delete_obj(collection='facilities', id=10, token=TOKEN):
  url = "/".join([ROOT_URL, collection, str(id), ""])
  return api_op(requests.delete, url, token=token)





