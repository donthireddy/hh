#!/usr/bin/env python3
import os, sys
os.environ['DJANGO_SETTINGS_MODULE'] = 'mcp.settings'
from django.conf import settings
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
import tennis.models as M
import datetime
import pytz
import argparse

tz=pytz.timezone('America/New_York')
parser = argparse.ArgumentParser(description="Create new Hacker user");
parser.add_argument('--username', type=str)
parser.add_argument('--password', type=str)
args = parser.parse_args()
n=M.User.objects.filter(username=args.username)[0]
n.last_login=tz.localize(n.last_login)
n.date_joined=tz.localize(n.date_joined)
n.set_password( args.password or args.username)
n.save()


