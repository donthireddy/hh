#!/usr/bin/env python3
import os, sys
os.environ['DJANGO_SETTINGS_MODULE'] = 'mcp.mcp.settings'
from django.core.wsgi import get_wsgi_application
from django.conf import settings
import tennis.models as M
from tennis.bracket_util import *
import math 
import itertools as it

application = get_wsgi_application()


DO_SINGLES=0
DO_DOUBLES=1

DFLT_DATE='2015-09-19'
if DO_SINGLES:
  SINGLES_LEAGUE_ID=0
  SINGLES_PLAYOFF_LEAGUE_ID=0

if DO_DOUBLES:
  DOUBLES_LEAGUE_ID=18
  DOUBLES_PLAYOFF_LEAGUE_ID=19

PLAYOFF_ENTRIES=8
INJURED_PLAYERS = set() # [M.Player.objects.get(user__last_name='Botsch')])


#################

bye_list = M.Player.objects.filter(user__last_name='bye',user__first_name='')
assert bye_list, "There is no player in database representing 'bye'. Need a user with last name bye and first name ''"
bye = bye_list[0]

def add_smatch(l, h1, a1, r, mn, date):
    import tennis.models as m
    if a1 == None:
        a1=bye
        home_won=True
    else:
        home_won=None
    m.Match(league=l,
            home_player1=h1,
            away_player1=a1,
            round_num = r,
            match_num = mn,
            home_won=home_won,
            date=date).save()

def add_dmatch(l, h1, h2, a1, a2, r, mn, date):
    import tennis.models as m

    m.Match(league=l,
            home_player1=h1,
            home_player2=h2,
            away_player1=a1,
            away_player2=a2,
            round_num = r,
            match_num = mn,
            date=date).save()

def next_round_list(pl):
    n = len(pl)
    round_n = math.ceil(math.log(n)/math.log(2)) + 1
    N = int(2 ** round_n)
    new_pl =[]
    for (i,p) in enumerate(pl):
        opponent = N - 1 - p
        if i % 2 == 0:
            new_pl.extend((opponent,p))
        else:
            new_pl.extend((p,opponent))
    return new_pl


def get_playoff_matchups(league_id):
    league = M.League.objects.get(id=league_id)

def flat_bracket(l):
   if type(l[0][0]) != list:
       return l
   res=[]
   for i in l:
       if type(i) == list:
           res=res+flat_bracket(i)
       else:
           res.append(i)
   return res


def main():
  
  cutoff = 0 
  doubles_league = M.League.objects.get(id=DOUBLES_LEAGUE_ID)
  doubles_playoff_league = M.League.objects.get(id=DOUBLES_PLAYOFF_LEAGUE_ID)
 
  if DO_SINGLES: 
    singles_league = M.League.objects.get(id=SINGLES_LEAGUE_ID)
    singles_playoff_league = M.League.objects.get(id=SINGLES_PLAYOFF_LEAGUE_ID)
    matches=M.Match.objects.filter(league=singles_playoff_league).order_by('date', 'round_num', 'match_num')
    for match in matches:
        print match.home_name()
        print match.away_name()
        print
  
    playoff_teams = [i[0]
                     for i in singles_league.standings()
                     if i[1].points >= cutoff and not 
                     INJURED_PLAYERS.intersection(i[0])
                     ]
    if len(playoff_teams) > PLAYOFF_ENTRIES:
       playoff_teams = playoff_teams[:PLAYOFF_ENTRIES]
  
    m_ix=0
    for m in flat_bracket(matchups(playoff_teams)):
      h1, = list(m[0])
      if m[1]:
          v1, = list(m[1])
      else:
          v1 =None
      m_ix = m_ix+1
      print 's', h1, v1, 1, m_ix
      add_smatch(singles_playoff_league, h1,v1, 1, m_ix, DFLT_DATE)
  
  
  print ""
  if DO_DOUBLES:  
    playoff_teams = [i[0]
                   for i in doubles_league.standings()
                   if i[1].points >= cutoff and not 
                   INJURED_PLAYERS.intersection(i[0])
                   ]
    if len(playoff_teams) > PLAYOFF_ENTRIES:
      playoff_teams = playoff_teams[:PLAYOFF_ENTRIES]
  
    m_ix=0
    for m in flat_bracket(matchups(playoff_teams)):
      h = list(m[0])
      h1 = h[0]
      if len(h)>1:
          h2=h[1]
      else:
          h2=h1

      v = list(m[1])
      v1 = v[0]
      if len(v)>1:
          v2=v[1]
      else:
          v2=v1

      m_ix = m_ix+1
      add_dmatch(doubles_playoff_league, h1,h2, v1,v2, 1, m_ix, DFLT_DATE)
      print 3, h1, h2, v1, v2, 1, m_ix


main()
