#!/usr/bin/env python
import csv, json
lineup = [row for row in csv.reader(open("lineup.txt"), delimiter=" ")]

lineup_dict = {"Doubles": [  [ [r[0],r[1]], [r[2],r[3]]]
                             for r in lineup
                             if len(r) == 4 ],
               "Singles": [  [r[0],r[1]]
                             for r in lineup
                             if len(r) == 2 ]
             }

json_data = json.dumps(lineup_dict)
print("""
curl -H "Content-Type: application/json" -X POST -d '{}'  http://hh.mariandrive.com/tennis/create_matches/
""".format(json_data))

