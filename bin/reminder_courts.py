#!/usr/bin/env python3
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'mcp.settings'

import django
django.setup()

from util.mail import send_mail
import datetime as dt
days=['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
def get_hl_date(today=dt.date.today()):

  dow=today.weekday()
  if dow in (0,1):
     day='Thursday'
     date = today + dt.timedelta(days=3-dow)
  else:
     day='Tuesday'
     date = today + dt.timedelta(days=8-dow)
  return day, date
day, date = get_hl_date()

body='''
Please reserve a court for Hackers League match on %s, %s
<br>

''' % (day,str(date))
send_mail(
    to=('maverickone@gmail.com','cmraje@yahoo.com','mikeklein1@yahoo.com',
        'harish_n@hotmail.com', 'rdonthireddy@yahoo.com', 'nizar@damani4.com'),
    subject='Reminder to reserve court for ' + day, body=body)

