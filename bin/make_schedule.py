#!/usr/bin/env python3
import os, sys
os.environ['DJANGO_SETTINGS_MODULE'] = 'mcp.settings'
from django.conf import settings
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
import tennis.models as M
from tennis.bracket_util import *
import re
import math 
import string
import itertools as it
import datetime as dt
import argparse

parser = argparse.ArgumentParser(description="Make Tennis legue schedule");
parser.add_argument('--date_begin', default='20160822')
parser.add_argument('--earliest_round', default=1, type=int)
parser.add_argument('--start_match_num', default=960, type=int)
parser.add_argument('--max_teams',default=6, type=int)
args = parser.parse_args()

start_date           = dt.datetime.strptime(args.date_begin,"%Y%m%d").date()
START_ROUND          = args.earliest_round
match_num            = args.start_match_num
MAX_TEAMS_PER_GROUP  = args.max_teams

WEEK=dt.timedelta(7)

def match_range(wno):
    return start_date + wno*WEEK, start_date + wno*WEEK + dt.timedelta(6)


def match_week(s,e):
      return format(s) + " - " + format(e)



league_ids = {
    "Singles" : 23,
    "Doubles" : 22,
}

is_singles = {"Singles": True, "Doubles": False}

reg_2016 = {
  "Singles" : [
      ["harish"],# | 3.58 |
      ["rob"],# | 3.55 |
      ["madhu"],# | 3.55 |
      ["guru"],# | 3.53 |
      ["andrew"],# | 3.50 |
      ["selvaraj"],# | 3.50 |
      ["ramd"],# | 3.44 |
      ["jeff"],# | 3.37 |
      ["sunil"],# | 3.30 |
      ["subrato"],# | 2.95 |
      ["sreenivas"],# | 2.88 |
  ],

  "Doubles": [

      ["rob",	"mahendra"], #A 	6.85
      ["subhrendu",	"mikek"], #B	6.84
      ["selvaraj",	"manoj"], #B	6.74
      ["guru",	"shankar"], #A 	6.73
      ["sai",	"RaviB"], #A 	6.72
      ["ramk",	"subrato"], #B	6.69
      ["sraman",	"avi"], #B	6.65
      ["nizar",	"sunil"], #A 	6.65
      ["murali",	"cm"], #A 	6.47
      ["harish",	"naveen"], #B	6.44
      ["bryan",	"ed"], #B	6.42

  ]
}

registrations = {
  "Singles" : [ 
      ["harish"],# | 3.58 | 
      ["rob"],# | 3.55 | 
      ["guru"],# | 3.53 | 
      ["jeff"],# | 3.37 | 
      ["sunil"],# | 3.30 | 
      ["vijay"],# 
  ], 

}
MAX_MATCHES=3


def is_even(n): 
  return n%2==0

def get_nweeks(N):
   if  is_even(N):
      return MAX_MATCHES
   else:
      return MAX_MATCHES+1
   
def make_schedule(players, is_odd):
    '''
    Algorithm copied from
    http://stackoverflow.com/questions/6648512/scheduling-algorithm-for-a-round-robin-tournament
    A better reference maybe:
    http://en.wikipedia.org/wiki/Round-robin_tournament#Scheduling_algorithm

    '''
    N=len(players)
    if N % 2 == 1:
        N += 1
    top=list(range(int(N/2)))
    bottom= list(reversed( range(int(N/2),int(N))))
    #print top, bottom

    nweeks = MAX_MATCHES +1  if is_odd else MAX_MATCHES
    n=0
    #print top;print bottom
    schedule =[]
    while n < nweeks:
        top_right = top[-1]
        bottom_left = bottom[0]
        # Rotate all but first element of top to right
        top[2:] = top[1:-1]
        # Assign previous bottom left player
        # to second element of top
        top[1] = bottom_left
        # Rotat bottom row to left
        bottom[:-1] = bottom[1:]
        # fill right of bottom with previous top right player
        bottom[-1] = top_right
        schedule.append(list(zip(top, bottom)))
        #print;print top;print bottom

        n = n+1 

    return schedule


bye=M.Player.objects.get(user__username="bye")

registrations_clean = {}
for flight, plist in registrations.items():
    team_list=[]
    for team in plist:
        partners=[]
        for p in team:
            print (p, end="")
            pobj = M.Player.objects.get(user__username = p.lower())
            partners.append(pobj)
            #print pobj.id
        team_list.append(partners)

    nteams = len(team_list)
    #print flight, nteams
    gsize=MAX_TEAMS_PER_GROUP
    groups = string.ascii_uppercase[:int(math.ceil(nteams/1.0/gsize))]

    group_assignments = (groups + groups[-1:-99:-1])*99
    #print group_assignments, team_list
    group_dict = dict([(k[0], [i[1] for i in l])
                       for k,l in it.groupby(sorted(zip(group_assignments, team_list)), lambda k:k[0])])
    #print group_dict
    registrations_clean[flight] = group_dict

#print registrations_clean        
#exit(0)
def team_name(t):
    name = t[0].user.username
    if len(t) > 1:
        name += " & " + t[1].user.username
    return str(name)

for flight, groups in sorted(registrations_clean.items()):
    league_id = league_ids[flight]
    print (flight, league_id)
    league = M.League.objects.get(id=league_id)
    for group, plist in sorted(groups.items()):
        nteams = len(plist)
        if nteams % 2 == 1:
            plist.append([bye,bye])
        sched = make_schedule(plist, nteams % 2 == 1)
        #print flight, group, plist

        for wno, week_matches in enumerate(sched):
            round_num = START_ROUND + wno
            for match in week_matches:
                h, a = [plist[i] for i in match]
                sd, ed = match_range(wno)
                print (",".join([flight, group,]+ [format(i) for i in (sd,ed)] + [team_name(h), team_name(a)]))
                if sd < dt.date(2016,9,13):
                  if is_singles[flight]:
                    # singles
                    M.Match(league=league,
                            home_player1=h[0],
                            away_player1=a[0],
                            round_num=round_num,
                            match_num=match_num,
                            date=sd).save()
                  else:
                    # doubles
                    M.Match(league=league,
                            home_player1=h[0],
                            home_player2=h[1],
                            away_player1=a[0],
                            away_player2=a[1],
                            round_num=round_num,
                            match_num=match_num,
                            date=sd).save()
                match_num += 1


