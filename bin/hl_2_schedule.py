#!/usr/bin/env python
import json
import requests
import sys
import os

SCHED_DATA="scheduling_data.json"
LINEUP_DATA="lineup.json"

url="http://hh.mariandrive.com/tennis/get_lineups/"
# Get JSON data
data = json.load(open(SCHED_DATA))

print ("finding lineup")
resp = requests.post(url, json=data)
out_data = json.loads(resp.text)

# Write to file
json.dump(out_data, open(LINEUP_DATA, "w"), indent=4)


#curl -H "Content-Type: application/json" -X POST -d "@$SCHED_DATA" http://hh.mariandrive.com/tennis/get_lineups/ > $LINEUP_DATA

