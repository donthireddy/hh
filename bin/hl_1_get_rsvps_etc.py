#!/usr/bin/env python
import json
import requests
import sys
import os

def process_rsvps(data):
  
  return data

# First argument is JSON file to create
file = sys.argv[1] if len(sys.argv)> 1 else "scheduling_data.json"

# URL to get data from
url = 'https://hh.mariandrive.com/tennis/sched/'
params = dict(
)

# Get JSON data
resp = requests.get(url=url, params=params)
data = json.loads(resp.text)

data= process_rsvps(data)

# Write to file
json.dump(data, open(file, "w"), indent=4)

print("Got scheduling data in {}".format(file))

