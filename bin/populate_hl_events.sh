#!/usr/bin/zsh
DAYS=( Tue Thu )
START_DATE=2017-04-16
FIRST_TUESDAY=$(echo "${START_DATE}\ +"{0..6}'\ days' |xargs -n1 date +'%a %Y-%m-%d' -d|grep Tue|sed -e 's/Tue //')
NWEEKS=15

for week in {0..$NWEEKS}
do 
  hweek=$(( week + 1 ))
  tue_date=$(date -d "$FIRST_TUESDAY + $week weeks" +%Y-%m-%d)
  thu_date=$(date -d "$FIRST_TUESDAY + $week weeks 2 days" +%Y-%m-%d)
  echo ./add_event.py --event_type HL --datetime \"$tue_date 15:30\" --name \"Hackers League 2017 Week $hweek Tue\"
  echo ./add_event.py --event_type HL --datetime \"$thu_date 15:30\" --name \"Hackers League 2017 Week $hweek Thu\"
done
