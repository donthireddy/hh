#!/usr/bin/env python
import json
import requests
import sys
import os
import re
import random
import django
from util.mail import send_mail
import tennis.models as M
import argparse
os.environ['DJANGO_SETTINGS_MODULE'] = 'mcp.settings'
django.setup()

# First argument is JSON file to create
SCHED_DATA = "scheduling_data.json"
LINEUP_DATA="lineup.json"

# URL to get scheduling data from
sched_url = 'https://hh.mariandrive.com/tennis/sched/'
# 
lineup_url="http://hh.mariandrive.com/tennis/get_lineups/"
# URL to get data from
create_match_url = 'http://hh.mariandrive.com/tennis/create_matches/'

parser = argparse.ArgumentParser(description="Create Hackers League lineup");
parser.add_argument('--event_id', type=int)
args = parser.parse_args()
if not args.event_id:
   print ("Plese provide event_id")
   sys.exit(1)

print("Event", args.event_id)

e=M.Event.objects.filter(id=args.event_id)[0]

def process_rsvps(data):
  return data

params = dict(
)

# Get JSON scheduling data
resp = requests.get(url=sched_url, params=params)
data = json.loads(resp.text)

sched_data= process_rsvps(data)

print("Got scheduling data ")

print ("finding lineup")
resp = requests.post(lineup_url, json=sched_data)
lineup_data = json.loads(resp.text)

params = dict(
)

resp = requests.post(create_match_url, json=lineup_data)

print("create_matches response = {code}, data={data}".format(code=resp.status_code, data=resp.json()))

reservers = {
"MD": "Donthireddy",
"MK": "Klein",
"CM": "Raje",
"ND": "Damani",
"HN": "Nankani",
"RD": "RDonthireddy",
}

members = M.Organization.objects.get(id=1).members_group.user_set.filter(
    is_active=True).order_by('email')
recipients = [p.email for p in members]

def match_desc(m):
    balls= -1

    def decorate(p, i, ball_guy):
       if i == ball_guy:
          ret= "*<strong>" + p.name + "</strong>"
       else: 
          ret = p.name
       return "<td> " + ret


#  
    if m.home_player2:
       bg = random.choice(range(4))
       return (decorate(m.home_player1, 0, bg) + 
            decorate(m.home_player2, 1, bg) +
            "<td> vs" +
            decorate(m.away_player1, 2, bg) +
            decorate(m.away_player2, 3, bg)) 
    else:
       bg = random.choice(range(2))
       return (decorate(m.home_player1, 0, bg) + 
            "<td> <td> vs" +
            decorate(m.away_player1, 1, bg) )

matches= [match_desc(m) for m in M.Match.objects.filter(date=e.date)]
print("Matches:")
print("\n".join(matches))

def get_reservation(s):
   m=re.match("([a-zA-Z]+)(.*)", s)
   reserver, time = m.group(1), m.group(2).strip()
   return reserver, time
 
courts=[]
m = re.search("^Cts:(.*)", e.comment)
if m:
  courts = [(reservers.get(r[0],r[0]), r[1]) for r in [get_reservation(x) for x in m.group(1).split(",")]]

if len(courts) < len(matches):
  print("Not enough courts")
  sys.exit(1)

matches_txt =( 
  "<table>" + 
    "<tr>".join(
      [ " <td> Court " + m[0][0] +
        " <td> " + (m[0][1] or e.date.strftime("%-I:%M %p")) +
        " <td> " + m[1]
       for m in zip(courts, matches)]) +
   "<table>")

subject = "HL Matches @ MCP {dt}".format(dt=e.date.strftime("%a %m/%d/%y"))

body="""
    Players whose names are in *<strong>bold letters with an asterisk</strong> 
    are responsible for bringing a can of balls.<br>

    Note that Ball responsibility is assigned randomly by the computer to one of four (or two) players
    in each match.
    <p><p>
    {matches}
    <p>
    <a href=http://hh.mariandrive.com> Hopewell Hackers</a> OC
    """.format(matches=matches_txt)

print("Sending email {subj} to {n} members\n {body}".format(n=len(recipients), subj=subject, body=body))
#send_mail(subject, body, to=("donthireddy@yahoo.com",))
send_mail(subject, body, to=recipients)

