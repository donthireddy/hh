#!/usr/bin/env python
import json
import requests
import sys
import os


# First argument is JSON file to get lineups from
file = sys.argv[1] if len(sys.argv) > 1 else "lineup.json"

# URL to get data from
url = 'http://hh.mariandrive.com/tennis/create_matches/'
params = dict(
)

# Get JSON data
data = json.load(open(file))

resp = requests.post(url, json=data)

print("create_matches response = {code}, data={data}".format(code=resp.status_code, data=resp.json()))

