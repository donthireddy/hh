#!/usr/bin/env python3
import os, sys
os.environ['DJANGO_SETTINGS_MODULE'] = 'mcp.settings'
from django.conf import settings
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
import tennis.models as M
import argparse

hackers=M.Group.objects.filter(name='Hopewell USTA Team')[0]
parser = argparse.ArgumentParser(description="Create new Hacker user");
parser.add_argument('--username', type=str)
parser.add_argument('--first_name', type=str)
parser.add_argument('--last_name', type=str)
parser.add_argument('--email', type=str)
parser.add_argument('--mobile', type=str)
parser.add_argument('--format_pref', default='D', type=str)
args = parser.parse_args()

user =M.User(is_superuser=True, username=args.username,
   first_name=args.first_name, last_name=args.last_name,email=args.email,is_staff=True, is_active=True)
user.set_password(args.username)
user.save()
user.groups.add(hackers)

user.save()
# Player should have been created by user's post save trigger
player=M.Player.objects.filter(user__id=user.id)[0]
player.mobile_phone=args.mobile
player.format_preference= args.format_pref

player.save()

# XXX TODO Registrations

