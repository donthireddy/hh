app = angular.module 'tennis.app.resource', ['tennis.api']

app.controller 'AppController', ['$scope', 'Player', ($scope, Player) ->
    $scope.players = Player.query()
]

app.controller 'AppController', ['$scope', 'User', ($scope, User) ->
    $scope.users = User.query()
]
