from rest_framework import serializers
import tennis.models as M

class FacilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = M.Facility
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = M.User
        fields = ('id', 'username')


class PlayerSerializer(serializers.ModelSerializer):
    #posts = serializers.HyperlinkedIdentityField('posts', view_name='userpost-list', lookup_field='username')
    #user = serializers.RelatedField()
    user = UserSerializer(required=True)
    #user = serializers.HyperlinkedRelatedField('user', view_name='user-detail',lookup_field='id')
                                                
    class Meta:
        model = M.Player
        fields = ('id', 'user', 'mobile_phone', 'work_phone', 'home_phone' )

class LeagueSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = M.League
        fields = '__all__'

class MatchSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = M.Match
        fields = '__all__'

class PPrefsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = M.PPrefs
        fields = '__all__'

class OrganizationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = M.Organization
        fields = '__all__'

class RegistrationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = M.Registration
        fields = '__all__'

class CourtTimeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = M.CourtTime
        fields = '__all__'

class EventSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = M.Event
        fields = '__all__'

class EventRsvpSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = M.EventRsvp
        fields = '__all__'

