from django.conf.urls import include, url
#from django.views.generic import TemplateView      
#from tennis.views import PersonListView
from django.views.generic import DetailView, ListView
from django.views.generic import TemplateView
from rest_framework import viewsets, routers
import tennis_api.api as api
from rest_framework.routers import DefaultRouter
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.authtoken import views as authtoken_views
from rest_framework_swagger.views import get_swagger_view
from rest_framework.schemas import get_schema_view
import rest_auth

schema_view = get_schema_view(title='Hackers API')

router = DefaultRouter()
router.register(r'players', api.PlayerViewSet)
router.register(r'users', api.UserViewSet)
router.register(r'facilities', api.FacilityViewSet)
router.register(r'leagues', api.LeagueViewSet)
router.register(r'matches', api.MatchViewSet)
router.register(r'pprefs', api.PPrefsViewSet)
router.register(r'organizations', api.OrganizationViewSet)
router.register(r'registrations', api.RegistrationViewSet)
router.register(r'courttimes', api.CourtTimeViewSet)
router.register(r'events', api.EventViewSet)
router.register(r'eventrsvps', api.EventRsvpViewSet)

swagger_view = get_swagger_view("Hackers API")

class SimpleStaticView(TemplateView):
    def get_template_names(self):
        return [self.kwargs.get('template_name') + ".html"]
    
    def get(self, request, *args, **kwargs):
        from django.contrib.auth import authenticate, login
        if request.user.is_anonymous():
            # Auto-login the User for Demonstration Purposes
            user = authenticate()
            login(request, user)
        return super(SimpleStaticView, self).get(request, *args, **kwargs)


urlpatterns = [
    # django-rest-framework
    url(r'^s/$', swagger_view),
    url(r'^schema/$', schema_view),
#    url(r'^rest-auth/registration/', include("rest_auth.registration.urls")),
    url(r'^token-auth/', authtoken_views.obtain_auth_token),
    url(r'^auth/', include('rest_framework.urls', namespace='rest_framework')),
#    url(r'^rest-auth/facebook/$', api.FacebookLogin.as_view(), name='fb_login'),
#    url(r'^rest-auth/twitter/$', api.TwitterLogin.as_view(), name='twitter_login'),
    url(r'^rest-auth/', include("rest_auth.urls")),
    url(r'^(?P<template_name>\w+)$', SimpleStaticView.as_view(), name='tennis_api'),
]

urlpatterns = [url(r'^', include(router.urls))] + format_suffix_patterns(urlpatterns)
#urlpatterns =  format_suffix_patterns(urlpatterns)

