from .serializers import (UserSerializer, PlayerSerializer, FacilitySerializer,
    MatchSerializer, PPrefsSerializer, OrganizationSerializer, RegistrationSerializer, 
    LeagueSerializer, CourtTimeSerializer, EventSerializer, EventRsvpSerializer)
from tennis.models import (User, Player, Facility, Match, PPrefs, Organization, Registration, 
    League, CourtTime, Event, EventRsvp)

from .permissions import IsOwnerOrReadOnly

from rest_framework import viewsets, generics, permissions
from rest_framework.decorators import detail_route
from rest_framework import renderers

#from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
#from rest_auth.registration.views import SocialLoginView
#from allauth.socialaccount.providers.twitter.views import TwitterOAuthAdapter
#from rest_auth.views import LoginView
#from rest_auth.social_serializers import TwitterLoginSerializer

#class FacebookLogin(SocialLoginView):
#    adapter_class = FacebookOAuth2Adapter

#class TwitterLogin(LoginView):
#    serializer_class = TwitterLoginSerializer
#    adapter_class = TwitterOAuthAdapter


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated, )


class PlayerViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = Player.objects.all()
    serializer_class = PlayerSerializer
    permission_classes = (permissions.IsAuthenticated, )# OrReadOnly, IsOwnerOrReadOnly,)

class MatchViewSet(viewsets.ModelViewSet):
    queryset = Match.objects.all()
    serializer_class = MatchSerializer
    permission_classes = (permissions.IsAuthenticated, )

class LeagueViewSet(viewsets.ModelViewSet):
    queryset = League.objects.all()
    serializer_class = LeagueSerializer
    permission_classes = (permissions.IsAuthenticated, )

# example code from DRF tutorial
#    @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
#    def highlight(self, request, *args, **kwargs):
#        player = self.get_object()
#        return Response(player.highlighted)

#    def perform_create(self, serializer):
#        serializer.save(owner=self.request.user)



class FacilityViewSet(viewsets.ModelViewSet):
    queryset = Facility.objects.all()
    serializer_class = FacilitySerializer
    permission_classes = (permissions.IsAuthenticated, )# OrReadOnly, IsOwnerOrReadOnly,)


class PPrefsViewSet(viewsets.ModelViewSet):
    queryset = PPrefs.objects.all()
    serializer_class = PPrefsSerializer
    permission_classes = (permissions.IsAuthenticated, )

class OrganizationViewSet(viewsets.ModelViewSet):
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer
    permission_classes = (permissions.IsAuthenticated, )

class RegistrationViewSet(viewsets.ModelViewSet):
    queryset = Registration.objects.all()
    serializer_class = RegistrationSerializer
    permission_classes = (permissions.IsAuthenticated, )

class CourtTimeViewSet(viewsets.ModelViewSet):
    queryset = CourtTime.objects.all()
    serializer_class = CourtTimeSerializer
    permission_classes = (permissions.IsAuthenticated, )

class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = (permissions.IsAuthenticated, )

class EventRsvpViewSet(viewsets.ModelViewSet):
    queryset = EventRsvp.objects.all()
    serializer_class = EventRsvpSerializer
    permission_classes = (permissions.IsAuthenticated, )

