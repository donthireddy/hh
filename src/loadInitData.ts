import debug_ = require("debug")
const debug = debug_("hh:cont:evtsnp")
import * as actions from "./actions"

export default (dispatch) => {
  debug(`Fetching facilities`)
  dispatch(actions.fetchFacilities())
  debug(`Fetching signups`)
  dispatch(actions.fetchEvtSignups())
  dispatch(actions.updateScratch({
    signups:[
      'peri', 'shankar', 'xxx007', 'xyz', 'murali','mrd','cm',
      'Nadal', 'sampras', 'borg', 'federer', 'djoker'//, 'murray'
    ]
  }))
}
