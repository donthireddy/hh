/**
 * Created by murali on 4/22/17.
 */
import {
  FETCH_EVT_SIGNUPS,
  FETCH_EVT_SIGNUPS_FULFILLED,
  FETCH_FACILITIES,
  FETCH_FACILITIES_FULFILLED,
  UPDATE_FACILITIES,
  UPDATE_SCRATCH,
} from "../consts";

export const updateScratch = (data) => ({type: UPDATE_SCRATCH,payload: data})
export const updateFacilities = (data) => ({type: UPDATE_FACILITIES,payload: data,})

export const fetchFacilities = () => ({ type: FETCH_FACILITIES, payload: {} });
export const fetchFacilitiesFulfilled = (payload) => ({ type: FETCH_FACILITIES_FULFILLED, payload });

export const fetchEvtSignups = () => ({ type: FETCH_EVT_SIGNUPS, payload: {} });
export const fetchEvtSignupsFulfilled = (payload) => ({ type: FETCH_EVT_SIGNUPS_FULFILLED, payload });
