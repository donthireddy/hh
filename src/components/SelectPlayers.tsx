import debug_ = require("debug")
const debug = debug_("hh:comp:fac")
import * as React from "react"
import ReactDOM from "react-dom"
import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc'
import RaisedButton from 'material-ui/RaisedButton'
import List from 'material-ui/List'
import {chunk, flatten} from '../util'
import * as R from 'ramda'

const itemStyle = {
  borderStyle:"solid",
  borderColor:"black",
  backgroundColor:'AquaMarine',
  borderWidth:"1px",
  margin: "0px",
}
const itemStyleOdd = R.merge(itemStyle,{
  color: "yellow",
  backgroundColor:"grey",
})

const topContainerStyle = {
  display: 'flex',
  flexDirection: 'row',
}
const playersContainerStyle = {
  width: "600px",
  display: 'flex',
  flexDirection: 'column',
  padding: 0,
}
const playerListStyle = {
  width: "100px",
  display: 'flex',
  flexDirection: 'column',
  padding: 0,
}
const matchStyle = {
  borderStyle: "solid",
  borderColor: "red",
  display:"flex",
  flexDirection: "row",
  borderWidth: "2px"}
const teamStyle = {
  borderStyle: "solid",
  borderColor: "green",
  borderWidth: "1px",
  display:"flex",
  flexDirection: "row",
  width: "50%",
}

// const ListItem = SortableElement(
//   ({value}) => <span style={itemStyle}> {value} </span>
// )
// const ListItemOdd = SortableElement(
//   ({value}) => <span style={itemStyleOdd}> {value} </span>
// )
const ListItem = style => SortableElement(
  ({value}) => <span style={style}> {value} </span>
)
const DoublesMatch = ({players, ix}) => {
  debug(`DoublesMatch`)
  return <div key={`DM${ix}`} style={matchStyle}>
    <div key={"homeTeam"} style = {teamStyle}>
       {players[0]}
       {players[1]}
    </div>
    <div key={"awayTeam"} style = {teamStyle}>
       {players[2]}
       {players[3]}
    </div>
  </div>

}

const SinglesMatch = ({players, ix}) => {
  debug(`SinglesMatch`)
  return <div key={`SM${ix}`} style={matchStyle}>
    <div key={"homePlyr"} style = {teamStyle}>
       {players[0]}
    </div>
    <div key={"awayPlyr"} style = {teamStyle}>
       {players[1]}
    </div>
  </div>

}

const renderGroupInDivs = (players, ix)  => {
  let res = []
  if (players.length ===4) {return <DoublesMatch key={`Dbl${ix}`} ix={ix} players={players}/>}
  if (players.length >=2) {res.push(<SinglesMatch key={`Sngls${ix}`} ix={ix} players={players}/>)}
  if (players.length % 2 == 1) {
    res.push(players[players.length - 1])
  }
  return res
}
const GROUP_SIZE = 4
const ListItemTeamEnd = ListItem(R.merge(itemStyle, {marginBottom: "1px"}))
const ListItemMatchEnd = ListItem(R.merge(itemStyle, {marginBottom: "2px"}))
const ListItemEven = ListItem(itemStyle)
const ListItemLone = ListItem(R.merge(itemStyle, { marginTop:"2px"}))
const renderGroupWithMarkup = (players, groupIx)  => {
  let baseIx = groupIx * GROUP_SIZE
  const grpSize = players.length
  switch(grpSize) {
    case 1:
      return [React.createElement(
        ListItem(R.merge(itemStyle, {backgroundColor: 'grey', marginTop:"2px"})),
        {key:`item-${baseIx}`, index:baseIx, value:players[0]})]
    case 2:
      return [
        React.createElement(
          ListItem(R.merge(itemStyle, {backgroundColor:'Cornsilk', marginBottom: "0px"})),
          {key:`item-${baseIx}`, index:baseIx, value:players[0]}),
        React.createElement(
          ListItem(R.merge(itemStyle, {backgroundColor:'Cornsilk', marginBottom: "10px"})),
          {key:`item-${baseIx+1}`, index:baseIx+1, value:players[1]}),
      ]
    case 3:
      return [
        React.createElement(
          ListItem(R.merge(itemStyle, {backgroundColor:'Cornsilk', marginBottom: "2px"})),
          {key:`item-${baseIx}`, index:baseIx, value:players[0]}),
        React.createElement(
          ListItem(R.merge(itemStyle, {backgroundColor:'Cornsilk', marginBottom: "10px"})),
          {key:`item-${baseIx+1}`, index:baseIx+1, value:players[1]}),
        React.createElement(
          ListItem(R.merge(itemStyle, {backgroundColor: 'LightGrey', marginTop:"2px"})),
          {key:`item-${baseIx+2}`, index:baseIx+2, value:players[2]})
      ]
    default:
      return [
        React.createElement(
          ListItem(itemStyle),
          {key:`item-${baseIx}`, index:baseIx, value:players[0]}),
        React.createElement(
          ListItem(R.merge(itemStyle, {marginBottom: "2px"})),
          {key:`item-${baseIx+1}`, index:baseIx+1, value:players[1]}),
        React.createElement(
          ListItem(itemStyle), //R.merge(itemStyle, {marginTop:"2px"})),
          {key:`item-${baseIx+2}`, index:baseIx+2, value:players[2]}),
        React.createElement(
          ListItem(R.merge(itemStyle, {marginBottom:"10px"})),
          {key:`item-${baseIx+3}`, index:baseIx+3, value:players[3]})
      ]
  }
}

const SelectPlayers = SortableContainer(
  ({availablePlayers}) =>
  <div style={playerListStyle}>
    <div> RSVPs</div>
    { R.pipe(
        chunk(GROUP_SIZE),
        R.addIndex(R.map)(renderGroupWithMarkup),
        flatten
      )(availablePlayers)
      //flatten(chunk(GROUP_SIZE, availablePlayers).map(renderGroupWithMarkup))
    }
  </div>)

const TwoViews = (props) => <div style={topContainerStyle}>
  <SelectPlayers {...props}/>
  <div style={playersContainerStyle }>
    <div> Results</div>
    {
      chunk(GROUP_SIZE, props.availablePlayers.map((val, ix) => (
        <RaisedButton primary={true} label={val} style={itemStyle}/>
      ))).map(renderGroupInDivs)
    }
  </div>
</div>
export default TwoViews;
