//@flow
import debug_ = require("debug")
const debug = debug_("hh:comp:fac")
//import {Table, Column, Cell} from 'fixed-data-table';
import "fixed-data-table/dist/fixed-data-table.css";
import * as React from "react"
import ReactDOM from "react-dom"

import ReactTable from "react-table";
import "react-table/react-table.css";

import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from "material-ui/Table";

const FacilitiesTable = ({data, columns, ...props}) =>
    <ReactTable data={data} columns={columns} showFilters={true}
                showPagination={false}
                defaultFilterMethod={(filter, row, column) => {
                    const id = filter.pivotId || filter.id
                    return row[id] !== undefined ? row[id].indexOf(filter.value) >= 0 : true
                }}
    />

const columnDefs = [
    {header: "Id", accessor: "id"},
    {header: "Abbrev", accessor: "abbrev"},
    {header: "Name", accessor: "name"},
];
const colDefs = [
    {headerName: "Id", field: "id"},
    {headerName: "Abbrev", field: "abbrev"},
    {headerName: "Name", field: "name"},
];
const Facilities = ({data, fetching}) => {
  //debug(`Got data: ${JSON.stringify(props)}`);
  return <div>
    {data && data.length > 0 ?
      <FacilitiesTable data={data} columns={columnDefs}/> :
      <span> Hello Facebook  - Murali</span>
    }
  </div>;
};

export default Facilities;
