import debug_ = require("debug")
const debug = debug_("hh:appmenu")
import AppBar from "material-ui/AppBar";
import FlatButton from "material-ui/FlatButton";
import IconButton from "material-ui/IconButton";
import IconMenu from "material-ui/IconMenu";
import MenuItem from "material-ui/MenuItem";
import NavigationClose from "material-ui/svg-icons/navigation/close";
import MoreVertIcon from "material-ui/svg-icons/navigation/more-vert";
import Toggle from "material-ui/Toggle";
import * as React from "react";
/**
 * Created by murali on 4/22/17.
 */
import { push as pushRoute } from "react-router-redux";

const Login = (props) => <FlatButton {...this.props} label="Login" />
function handleTouchTap() {
    alert("onTouchTap triggered on the title component");
}
type Logged = (any) => any;
// {muiName: string}
const Logged : Logged = (props) => (
  <IconMenu
    {...props}
    iconButtonElement={
      <IconButton><MoreVertIcon /></IconButton>
    }
    targetOrigin={{horizontal: "right", vertical: "top"}}
    anchorOrigin={{horizontal: "right", vertical: "top"}}
  >
    <MenuItem primaryText="Refresh" />
    <MenuItem primaryText="Help" />
    <MenuItem primaryText="Sign out" />
  </IconMenu>
);

// Logged.muiName = 'IconMenu';

const AppMenu = ({dispatch, ...props}) => {
  debug(`AppMenu got props: ${JSON.stringify(props)}`)
  return <IconMenu
    {...props}
    iconButtonElement={
      <IconButton><MoreVertIcon /></IconButton>
    }
    targetOrigin={{horizontal: "left", vertical: "top"}}
    anchorOrigin={{horizontal: "left", vertical: "top"}}
    onChange={(event, value) => dispatch(pushRoute(value))}
  >
    <MenuItem value="/react/fac" primaryText="Facilities"/>
    <MenuItem value="/react/evt" primaryText="Signups"/>
    <MenuItem value="/react/sel" primaryText="Select"/>
    <MenuItem value="/react/about" primaryText="About"/>
  </IconMenu>
};

// AppMenu.muiName = 'IconMenu';

/**
 * This example is taking advantage of the composability of the `AppBar`
 * to render different components depending on the application state.
 */
const HHTopBar = (props) =>
  <div>
    <Toggle
      label="Logged"
      defaultToggled={true}
      onToggle={debug(`login toggle`)}
      labelPosition="right"
      style={{margin: 20}}
    />
    <AppBar
      title="Hopewell Hackers Tennis Club"
      iconElementLeft={<AppMenu dispatch={props.dispatch}/>}
      iconElementRight={props.logged ? <Logged /> : <Login />}
    />
  </div>;

export default HHTopBar;
