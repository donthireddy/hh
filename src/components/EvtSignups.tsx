/**
 * Created by murali on 4/24/17.
 */
import debug_ = require("debug")
const debug = debug_("hh:comp:fac")
import * as React from "react"
import ReactDOM from "react-dom"
//import {Table, Column, Cell} from 'fixed-data-table';
import JSONTree from "react-json-tree";

import ReactTable from "react-table";
import "react-table/react-table.css";

const EvtSignups = ({data}) => <JSONTree data={ data } />

export default EvtSignups;
