/**
 * Created by murali on 4/22/17.
 */
import { routerReducer} from "react-router-redux"
import {combineReducers} from "redux";
import evtSignups from "./evtSignups";
import facilities from "./facilities";
import scratch from "./scratch";

export default combineReducers({
  facilities,
  evtSignups,
  scratch,
  router: routerReducer,
});
