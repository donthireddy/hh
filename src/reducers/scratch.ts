import * as consts from "../consts";
import debug_ = require("debug")
const debug = debug_("hh:rdcr:fac")

const initial = {signups:[]}
export default (state= initial, action) => {
  //debug(`Fac reducer: ${action.type} state:${JSON.stringify(state)}`);
  switch (action.type) {
    case  consts.UPDATE_SCRATCH:
      const newState = Object.assign({}, state, action.payload)
      debug(`UPDATE_SCRATCH: new state=${JSON.stringify(newState)}`);
      return newState;
  }
  //debug(`New state${JSON.stringify(newState)}`);
  return state;
}
