/**
 * Created by murali on 4/22/17.
 */
import * as Im from "immutable";
import * as consts from "../consts";
import debug_ = require("debug")
const debug = debug_("hh:rdcr:fac")
const initFac = Im.fromJS({counter: 0, data: [], fetching: false});
let counter = 0;
export default (state= initFac, action) => {
  //debug(`Fac reducer: ${action.type} state:${JSON.stringify(state)}`);
  let newState = state;
  counter++;
  switch (action.type) {
    case  consts.UPDATE_FACILITIES:
      //debug(`UPDATE_FACILITIES`);
      newState = state.set("data", Im.fromJS(action.payload));
      break
    case consts.FETCH_FACILITIES:
      newState = state.set("fetching", true);
      break
    case consts.FETCH_FACILITIES_FULFILLED:
      //debug(`FETCH_FACILITIES_FULFILLED: ${JSON.stringify(action.payload)}`);
      newState = state.set("fetching", false).set("data", Im.fromJS(action.payload));
      break
  }
  //debug(`New state${JSON.stringify(newState)}`);
  return newState.set("counter", counter);
}
