/**
 * Created by murali on 4/24/17.
 */
import * as Im from "immutable";
import * as consts from "../consts";
import debug_ from "debug"
const debug = debug_(`hh:rdcr:evtsnp`)

const initState = Im.fromJS({data: [], fetching: false});
export default (state= initState, action) => {
  //debug(`Fac reducer: ${action.type} state:${JSON.stringify(state)}`);
  let newState = state;
  switch (action.type) {
    case  consts.UPDATE_EVT_SIGNUPS:
      //debug(`UPDATE_EVT_SIGNUPS`);
      newState = state.set("data", Im.fromJS(action.payload));
      break
    case consts.FETCH_EVT_SIGNUPS:
      newState = state.set("fetching", true);
      break
    case consts.FETCH_EVT_SIGNUPS_FULFILLED:
      //debug(`FETCH_EVT_SIGNUPS_FULFILLED: ${JSON.stringify(action.payload)}`);
      newState = state.set("fetching", false).set("data", Im.fromJS(action.payload));
      break
  }
  //debug(`New state${JSON.stringify(newState)}`);
  return newState;
}
