/**
 * Created by murali on 4/22/17.
 */
import * as Colors from "material-ui/styles/colors";
import getMuiTheme from "material-ui/styles/getMuiTheme";
import Spacing from "material-ui/styles/spacing";
import zIndex from "material-ui/styles/zIndex";
import * as ColorManipulator from "material-ui/utils/colorManipulator";

export default getMuiTheme({
    spacing: Spacing,
    zIndex,
    fontFamily: "Roboto, sans-serif",
    // Color picking tool here: https://material.io/guidelines/style/color.html#
     palette: {
        primary1Color: Colors.blue700,
        primary2Color: Colors.deepPurple500,
        primary3Color: Colors.red500,
        accent1Color: Colors.pinkA200,
        accent2Color: Colors.grey100,
        accent3Color: Colors.grey500,
        textColor: Colors.blue900,
        alternateTextColor: Colors.white,
        canvasColor: Colors.white,
        borderColor: Colors.grey300,
        disabledColor: ColorManipulator.fade(Colors.darkBlack, 0.3),
        pickerHeaderColor: Colors.cyan500,
    },
});
