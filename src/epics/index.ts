/**
 * Created by murali on 4/23/17.
 */
import {ajax} from "rxjs/observable/dom/ajax";
import { fetchEvtSignupsFulfilled, fetchFacilitiesFulfilled,
} from "../actions";
import {FETCH_EVT_SIGNUPS, FETCH_FACILITIES} from "../consts";
import debug_ = require("debug")
const debug = debug_("hh:epics");
import { combineEpics } from "redux-observable";
import "rxjs/add/operator/map"

interface IFacility  {
  id: number,
  name: string,
  abbrev: string,
  phone: string,
  address: string,
  map_url: string
}
// epic
const fetchFacilitiesEpic = (action$) =>
  action$.ofType(FETCH_FACILITIES)
  .map((x) => {debug(`fetch facilities epic`); return x; })
  .mergeMap((action) =>
    ajax.getJSON<IFacility[]>(`/api/facilities/`)
    .map((response) => fetchFacilitiesFulfilled(response)),
  );

const fetchEvtSignupsEpic = (action$) =>
  action$.ofType(FETCH_EVT_SIGNUPS)
  .map((x) => {debug(`fetch evt sign up epic`); return x; })
  .mergeMap((action) =>
    ajax.getJSON(`/tennis/sched/`)
    .map((response) => fetchEvtSignupsFulfilled(response)),
  );

export default combineEpics(fetchFacilitiesEpic, fetchEvtSignupsEpic);
