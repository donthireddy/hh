import debug_ from "debug"
const debug = debug_(`hh:cfgStore`)
import { routerMiddleware} from "react-router-redux";
// import ReconnectingWebSocket from "reconnecting-websocket"
import {applyMiddleware, createStore} from "redux"
import { createLogger } from "redux-logger";
import { createEpicMiddleware } from "redux-observable";
import epics from "./epics";
import reducer from "./reducers" // Or wherever you keep your reducers
export default function configureStore(history) {
  // const ws_scheme = window.location.protocol === "https:" ? "wss" : "ws"
  // const url = ws_scheme + "://" + window.location.host + "/chat/hackers"// window.location.pathname)
  // const chatsock = new ReconnectingWebSocket(url)
  // debug(`connecting to websocket ${url}`)
  // chatsock.onmessage = function(message) {
  //   debug(`ws recvd: ${message}`)
  // }
  //
  // const message = {
  //   handle: "MRD",
  //   message: "Hello Channels",
  // }
  // chatsock.send(JSON.stringify(message));

  const middlewares = [
    routerMiddleware(history),
    createEpicMiddleware(epics),
    createLogger({}),
  ];
// Add the reducer to your store on the `router` key
// Also apply our middleware for navigating
  return createStore(
    reducer,
    applyMiddleware(...middlewares),
  )
}
