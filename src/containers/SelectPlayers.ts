
import {connect} from "react-redux"
import SelectPlayers from "../components/SelectPlayers";
import debug_ = require("debug")
const debug = debug_("hh:cont:evtsnp")
import { arrayMove} from 'react-sortable-hoc'
import * as actions from "../actions"


const add = (i:number) => (j:number) => i+j
debug(add(2)(3));

// declare let connect: any;
interface IOpt {pure: boolean}
interface ISignupData {singles_signups: string[], doubles_signups: string[]}

function getSignups(data: ISignupData) {
  let signups = []
  if (data  && data.singles_signups) {
    signups = data.singles_signups.concat(data.doubles_signups)
  }
  signups = signups.length ? signups :
  debug(`Signups today:${JSON.stringify(signups)}`)
  return signups
}
const onSortEnd = (signups, dispatch) => ({oldIndex, newIndex}) => {
  const newSignups = arrayMove(signups, oldIndex, newIndex)
  debug(`Got move ${oldIndex} -> ${newIndex}; ${JSON.stringify(signups)} -> ${JSON.stringify(newSignups)}`)
  dispatch(actions.updateScratch({signups: newSignups}))
}
const cont = connect<any, undefined, IOpt>(
  (state) => ({
      availablePlayers: state.scratch.signups, // getSignups(state.evtSignups.get("data").toJS()),
  }),
  (dispatch) => ({dispatch}),
  ({availablePlayers}, {dispatch}, otherProps) => ({
    ...otherProps, availablePlayers, onSortEnd: onSortEnd(availablePlayers, dispatch)
  }),
  {pure: false},
)(SelectPlayers);

export default cont
