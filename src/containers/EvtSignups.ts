/**
 * Created by murali on 4/24/17.
 */
import {connect} from "react-redux"
import EvtSignups from "../components/EvtSignups";
import debug_ = require("debug")
const debug = debug_("hh:cont:evtsnp")
// declare let connect: any;
interface IOpt {pure: boolean}

const cont = connect<any, undefined, IOpt>(
  (state) => {
    // debug(`mapStateToProps: ${JSON.stringify(state)}`);
    return {
      data: state.evtSignups.get("data").toJS() || [],
      fetching: state.evtSignups.get("fetching") || false,
    };
  },
  null,
  null,
  {pure: false},
)(EvtSignups);

export default cont;
