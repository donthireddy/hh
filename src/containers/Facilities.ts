/**
 * Created by murali on 4/23/17.
 */
import debug_ from "debug"
const debug = debug_(`hh:cont:fac`)
import {connect} from "react-redux";
import Facilities from "../components/Facilities"

const wrapped = connect(
  (state) => {
    // debug(`mapStateToProps: ${JSON.stringify(state)}`);
    return {
      data: state.facilities.get("data").toJS() || [],
      fetching: state.facilities.get("fetching") || false,
      pollInterval: 10000,
    };
  },
  null,
  null,
  {pure: false},
)(Facilities)

export default wrapped
