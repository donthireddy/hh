/**
 * Created by murali on 4/24/17.
 */

export const UPDATE_SCRATCH = "UPDATE_SCRATCH"

export const UPDATE_FACILITIES = "UPDATE_FACILITIES"
export const FETCH_FACILITIES = "FETCH_FACILITIES"
export const FETCH_FACILITIES_FULFILLED = "FETCH_FACILITIES_FULFILLED"

export const UPDATE_EVT_SIGNUPS = "UPDATE_FACILITIES"
export const FETCH_EVT_SIGNUPS = "FETCH_EVT_SIGNUPS"
export const FETCH_EVT_SIGNUPS_FULFILLED = "FETCH_EVT_SIGNUPS_FULFILLED"
