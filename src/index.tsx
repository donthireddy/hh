import debug_ = require("debug")
const debug = debug_("hh")
import createHistory from "history/createBrowserHistory"
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider"
import * as React from "react" // eslint-disable-line no-unused-vars
import * as ReactDOM from "react-dom"
import {Provider} from "react-redux"
import {Route} from "react-router"
import {ConnectedRouter} from "react-router-redux"
import * as injectTapEventPlugin from "react-tap-event-plugin"
import "rxjs"
import * as actions from "./actions"
import About from "./components/About"
import AppMenu from "./components/AppMenu"
import configureStore from "./configureStore"
import EvtSignupsContainer from "./containers/EvtSignups"
import FacilitiesContainer from "./containers/Facilities"
import SelectPlayersContainer from "./containers/SelectPlayers"
import themeHH from "./themeHH"
import loadInitData from "./loadInitData"
debug("Starting App")
// Create a history of your choosing (we"re using a browser history in this case)
const history = createHistory()
const store = configureStore(history)

loadInitData(store.dispatch)
/*
 Needed for onTouchTap
 http://stackoverflow.com/a/34015469/988941
 */
injectTapEventPlugin()

debug(`Rendering app`)
const App = () => (
  <Provider store={store}>
    <MuiThemeProvider muiTheme={themeHH}>
      <div>
        <AppMenu dispatch={store.dispatch}/>
        <ConnectedRouter history={history}>
          <div>
            <Route exact path="/react/" component={FacilitiesContainer}/>
            <Route path="/react/about" component={About}/>
            <Route path="/react/fac" component={FacilitiesContainer}/>
            <Route path="/react/evt" component={EvtSignupsContainer}/>
            <Route path="/react/sel" component={SelectPlayersContainer}/>
          </div>
        </ConnectedRouter>
      </div>
    </MuiThemeProvider>
  </Provider>
)

ReactDOM.render(<App/>, document.getElementById("container"))
