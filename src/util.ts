/**
 * given an array, breaks it up into chunks of requested size and returns an array of chunks
 * Eg: chunk([1,2,3,4,5,6,7], 3) -> [[1,2,3],[4,5,6],[7]]
 * @param  {[type]} i<n [description]
 * @return {[type]}     [description]
 */
import * as R from 'ramda'

const chunk_ = <T> (len:number, arr:Array<T>) : Array<Array<T>> => {
  let chunks = [],
      i = 0,
      n = arr.length;
  while (i < n) {
    chunks.push(arr.slice(i, i += len));
  }
  return chunks;
}
export const chunk = R.curry(chunk_)
export const flatten = <T>(arr: Array<Array<T>>) : Array<T>  =>
  [].concat.apply([], arr)
