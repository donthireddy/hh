#!/usr/bin/env python3

# 
def send_email(tolist, message):
    import smtplib
    user = 'maverickone@gmail.com'
    password = mypassword
    smtpserv="smtp.gmail.com:587"
    mailserver=smtplib.SMTP(smtpserv)
    mailserver.starttls()
    mailserver.login(user,password)
    mailserver.sendmail(user,tolist,message)

# %cpaste
def add_courttime(loc, num, date, time):
    import tennis.models as m
    loc = m.Facility.objects.get(abbrev = loc)
    m.CourtTime(location=loc, court_no=num, court_date=date,
                court_time=time).save()
    

def add_smatch(h1, a1, date):
    import tennis.models as m
    singles = m.League.objects.get(id=1)
    h1 = m.Player.objects.get(user__username=h1)
    a1 = m.Player.objects.get(user__username=a1)
    m.Match(league=singles,
            home_player1=h1,
            away_player1=a1, date=date).save()

def add_dmatch(h1, h2, a1, a2, date):
    import tennis.models as m
    doubles = m.League.objects.get(id=2)
    h1 = m.Player.objects.get(user__username=h1)
    h2 = m.Player.objects.get(user__username=h2)
    a1 = m.Player.objects.get(user__username=a1)
    a2 = m.Player.objects.get(user__username=a2)
    m.Match(league=doubles,
            home_player1=h1,
            home_player2=h2,
            away_player1=a1,
            away_player2=a2,
            date=date).save()




d='2013-09-14'
t='8:30'
add_courttime("HVTS", "#1", d,t)
add_courttime("HVTS", "#2", d,t)
add_courttime("HVTS", "#3", d,t)

t='10:30'
add_courttime("HVTS", "#1", d,t)
add_courttime("HVTS", "#2", d,t)
add_courttime("HVTS", "#3", d,t)

d='2013-09-15'
t='8:00'
add_courttime("HVTS", "#1", d,t)
add_courttime("HVTS", "#2", d,t)
add_courttime("HVTS", "#3", d,t)

t='10:00'
add_courttime("HVTS", "#1", d,t)
add_courttime("HVTS", "#2", d,t)
add_courttime("HVTS", "#3", d,t)
