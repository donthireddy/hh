#!/usr/bin/env python3
#import MySQLdb
import psycopg2
import json
from dotmap import DotMap
# default port 3306
PORT = 3307
SINGLES_LEAGUE_ID = 30
DOUBLES_LEAGUE_ID = 31
SINGLES = 'S'
DOUBLES = 'D'

def get_player_attrs(db):
    prefs = {}
    ratings = {}
    cursor = db.cursor()

    # execute SQL query using execute() method.
    try:
        cursor.execute(
            '''
                SELECT u.username player, u1.username pr1, u2.username pr2, u3.username pr3, rating_adj
                FROM tennis_pprefs pr
                JOIN tennis_player p ON pr.player_id=p.id JOIN auth_user u ON p.user_id=u.id
                LEFT JOIN tennis_player p1 ON pr.p1_id=p1.id LEFT JOIN auth_user u1 ON p1.user_id=u1.id
                LEFT JOIN tennis_player p2 ON pr.p2_id=p2.id LEFT JOIN auth_user u2 ON p2.user_id=u2.id
                LEFT JOIN tennis_player p3 ON pr.p3_id=p3.id LEFT JOIN auth_user u3 ON p3.user_id=u3.id
            ''')
        results = cursor.fetchall()
        for row in results:
            (player, pr1, pr2, pr3, rating_adj) = row
            # print(player, pr1, pr2, pr3, rating_adj)
            prefs[player] = (pr1, pr2, pr3)
            ratings[player] = 2.0+float(rating_adj)
    except RuntimeError as e:
        print("Error %s: unable to fecth player prefs" % (e,))
    return prefs, ratings


def get_signups(db):
    s = {}
    cursor = db.cursor()
    try:
        cursor.execute('''
            SELECT u.username, p.format_preference
            FROM   tennis_eventrsvp r
            JOIN   tennis_player p ON r.player_id = p.id JOIN auth_user u ON p.user_id = u.id
            WHERE  response IN ('A','I')
            AND    event_id = (SELECT id FROM tennis_event
                               WHERE  event_type='HL'
                               AND date = (SELECT min(date)
                                            FROM tennis_event
                                            WHERE event_type='HL'
                                              AND date>=now()))
        ''')
        for row in cursor.fetchall():
            s[row[0]] = row[1] or 'D'
    except RuntimeError as e:
        print("Error %s: unable to fecth signups" % (e,))

    return s


def get_prev_matches(db, league_id):
    # matches = []
    cursor = db.cursor()
    try:
        cursor.execute('''
        SELECT hp1u.username, hp2u.username, ap1u.username, ap2u.username
        FROM   tennis_league l
        JOIN   tennis_match m ON l.id = m.league_id
        JOIN      tennis_player hp1 ON m.home_player1_id=hp1.id      JOIN auth_user hp1u ON hp1.user_id=hp1u.id
        LEFT JOIN tennis_player hp2 ON m.home_player2_id=hp2.id LEFT JOIN auth_user hp2u ON hp2.user_id=hp2u.id
        JOIN      tennis_player ap1 ON m.away_player1_id=ap1.id      JOIN auth_user ap1u ON ap1.user_id=ap1u.id
        LEFT JOIN tennis_player ap2 ON m.away_player2_id=ap2.id LEFT JOIN auth_user ap2u ON ap2.user_id=ap2u.id
        WHERE l.id = %d
        ''' % (league_id,))
        return [row for row in cursor.fetchall()]
    except RuntimeError as e:
        print("Error %s: unable to fetch previos matches" % (e,))

    return []

def get_usta_signups():
    """
    Get players signed up for USTA matches next weekend.
    They need to get priority in scheduling hackers league matches
    """
    pass

########################################################################


def load_data(singles_league_id=30,
              doubles_league_id=31,
              dump=False):
    D = DotMap()
    conn = psycopg2.connect(host="127.0.0.1",
                           user="donthireddy", password="donthireddy",
                           dbname="tennis")
    D.singles_signups = []
    D.doubles_signups = []
    D.format_prefs = get_signups(conn)
    D.singles_signups = list(filter(lambda p: D.format_prefs[p] == 'S', D.format_prefs))
    D.doubles_signups = list(filter(lambda p: D.format_prefs[p] != 'S', D.format_prefs))
    D.partner_prefs, D.player_ratings = get_player_attrs(conn)
    D.prev_singles_matches = get_prev_matches(conn, SINGLES_LEAGUE_ID)
    D.prev_doubles_matches = get_prev_matches(conn, DOUBLES_LEAGUE_ID)
    if dump:
        json.dump(D, open("schedule.dat", "w"), indent=4, sort_keys=True)

    conn.close()
    return D

########################################################################
# Main
########################################################################
if __name__ == "__main__":
    D = load_data(dump=True)
