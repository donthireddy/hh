#!/bin/bash
source ~/.virtualenvs/dj16py27/bin/activate
export PYTHONPATH=~:~/mcp
ERR_LOG=/home/donthireddy/logs/user/error_gunicorn.log
ACC_LOG=/home/donthireddy/logs/user/access_gunicorn.log
PID=/home/donthireddy/logs/user/gunicorn.pid
PORT=18721
gunicorn --debug --daemon --pid $PID --access-logfile $ACC_LOG --error-logfile $ERR_LOG --bind "127.0.0.1:$PORT" mcp.wsgi:application
