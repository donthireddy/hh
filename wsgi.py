#activate_this = '/home/donthireddy/.virtualenvs/dj16py27/bin/activate_this.py'
#exec(open(activate_this).read(),dict(__file__=activate_this))

import os
import sys

os.environ['DJANGO_SETTINGS_MODULE'] = 'mcp.settings'

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
