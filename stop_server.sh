#!/bin/bash
PIDFILE=~/logs/user/gunicorn.pid
if [ -r $PIDFILE ];
then
  kill $(cat $PIDFILE)
else
  echo "gunicorn doesn't appear to be running"
fi
